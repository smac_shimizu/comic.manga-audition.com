<?php

/**
 * cache
 */
$GLOBALS['sma2013comic_cache'] = array();

/**
 * Turning off XML-RPC
 */
add_filter( 'xmlrpc_enabled', '__return_false' );

/**
 * 不要な wp_head 出力を消す
 */
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10 );
remove_action( 'wp_head', 'start_post_rel_link', 10 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_generator' );
//remove_action( 'wp_head', 'rel_canonical' );

/**
 * 不要なダッシュボードウィジェットの除去
 */
if (is_admin()) :
function sma2013comic_remove_dashboard_widgets(){
	//remove_meta_box('dashboard_right_now', 'dashboard', 'normal');   // Right Now
	//remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // Recent Comments
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
	remove_meta_box('dashboard_plugins', 'dashboard', 'normal');   // Plugins
	remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
	//remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
	remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog
	remove_meta_box('dashboard_secondary', 'dashboard', 'side');   // Other WordPress News
}
add_action('wp_dashboard_setup', 'sma2013comic_remove_dashboard_widgets');
endif;

/**
 * Disable comment flood protection
 */
add_filter('comment_flood_filter', '__return_false', 11);

/**
 * init
 */
function sma2013comic_init() {
	global $comic;
	$comic = array();
	$comic['settings'] =  array();

	$comic['settings']['entry_image_base_path'] = dirname(ABSPATH) . '/images/entries/';
	$comic['settings']['entry_image_base_url'] = home_url('/') . 'images/entries/';
	$comic['settings']['entry_image_uploads_tmp'] = dirname(ABSPATH) . '/images/uploads-tmp';
	$comic['settings']['imgresize_dir'] = WP_CONTENT_DIR . '/sh/';

	remove_theme_support('custom-background');
}
add_action('init', 'sma2013comic_init');

/**
 * Enqueues scripts and styles for front-end.
 */
function sma2013comic_scripts_styles() {
	wp_enqueue_style( 'sma2013comic-fonts-lobster', 'http://fonts.googleapis.com/css?family=Lobster', array(), null );
	wp_enqueue_style( 'sma2013comic-fonts-pt-mono', 'http://fonts.googleapis.com/css?family=PT+Mono', array(), null );
	wp_enqueue_style( 'sma2013comic-fonts-eb-garamond', 'http://fonts.googleapis.com/css?family=EB+Garamond', array(), null );
	wp_enqueue_style( 'sma2013comic-base', get_stylesheet_directory_uri() . '/base.css');
}
add_action( 'wp_enqueue_scripts', 'sma2013comic_scripts_styles');

function sma2013comic_scripts_styles_later_loads() {
	wp_dequeue_style( 'twentytwelve-fonts' );
}
add_action( 'wp_enqueue_scripts', 'sma2013comic_scripts_styles_later_loads', 99);

/**
 * Enqueues scripts and styles for back-end.
 */
function sma2013comic_admin_scripts_styles() {
	wp_enqueue_script( 'sma2013comic-admin', get_stylesheet_directory_uri() . '/js/admin-custom.js', array('jquery'), false, true);
}
add_action( 'admin_enqueue_scripts', 'sma2013comic_admin_scripts_styles', 11);

/**
 * 新しいユーザーロールを作る
 */
function sma2013comic_add_new_role() {
	//remove_role('extended_editor');
	$extended_editor = add_role(
		'extended_editor',
		'Editor + edit users',
		get_role('editor')->capabilities
	);
	if(null !== $extended_editor) {
		$extended_editor->add_cap('list_users');
		$extended_editor->add_cap('edit_users');
	}

	//remove_role('guest_reader');
	$guest_reader = add_role(
		'guest_reader',
		'Guest Reader'
	);
	if(null !== $guest_reader) {
		$guest_reader->add_cap('edit_others_posts');
	}
}
sma2013comic_add_new_role();

/**
 * editable_roles
 */
function sma2013comic_editable_roles($all_roles) {
	// extended_editor
	if(current_user_can('extended_editor') && isset($all_roles['subscriber'])) {
		$subscriber = $all_roles['subscriber'];
		$all_roles = array();
		$all_roles['subscriber'] = $subscriber;
	}

	return $all_roles;
}
add_filter('editable_roles', 'sma2013comic_editable_roles');

/**
 * 特定のユーザーロールによる特定のユーザー情報編集を制限する
 */
function sma2013comic_restrict_user_edit() {
	global $pagenow;

	if(!isset($pagenow) || 'user-edit.php' != $pagenow || !isset($_GET['user_id']))
		return;

	$user_id_to_edit = (int) $_GET['user_id'];

	// extended_editor can only edit subscriber.
	if(current_user_can('extended_editor') && !user_can($user_id_to_edit, 'subscriber'))
		wp_die(__('You do not have permission to edit this user.'));
}
add_action('admin_init', 'sma2013comic_restrict_user_edit');

/**
 * デフォルトのメールアドレス変更
 */
function sma2013comic_wp_mail_from($mail = '') {
	return 'entries@manga-audition.com';
}
add_action('wp_mail_from', 'sma2013comic_wp_mail_from');

/**
 * デフォルトのメール名義変更
 */
function sma2013comic_wp_mail_from_name($name = '') {
	return 'Silent Manga Audition';
}
add_action('wp_mail_from_name', 'sma2013comic_wp_mail_from_name');

/**
 * entry-category タクソノミーの編集画面に独自カラムを追加
 */
function sma2013comic_manage_edit_entry_category_columns($columns) {
	return array_merge($columns, array('active' => 'Active'));
}
add_filter('manage_edit-entry-category_columns', 'sma2013comic_manage_edit_entry_category_columns');

function sma2013comic_manage_entry_category_custom_column($string, $column_name, $term_id) {
	if('active' == $column_name) {
		global $sma2013comic_cache;

		if(isset($sma2013comic_cache['manage_entry_category_custom_column_entry_category_fields']))
			$fields = $sma2013comic_cache['manage_entry_category_custom_column_entry_category_fields'];
		else {
			$fields = sma2013comic_get_entry_category_fields(true, 'term_id');
			$sma2013comic_cache['manage_entry_category_custom_column_entry_category_fields'] = $fields;
		}

		$string = isset($fields[$term_id]) ? 'Yes' : 'No';
	}

	return $string;
}
add_filter('manage_entry-category_custom_column', 'sma2013comic_manage_entry_category_custom_column', 10, 3);

/**
 * entry-category タクソノミーのカスタムフィールド含むすべてのフィールドを取得
 */
function sma2013comic_get_entry_category_fields($hide_inactive = true, $key = 'slug') {
	global $comic;
	$fields = array();
	$terms = get_terms('entry-category', array('hide_empty' => false));

	if(empty($terms) || is_wp_error($terms))
		return $fields;

	foreach ($terms as $term) {
		if(isset($term->term_id) && isset($term->slug)) {
			$term_fields = (array) $term;
			$custom_fields = get_fields('entry-category_' . $term->term_id);
			$merged = !empty($custom_fields) ? array_merge($term_fields, (array) $custom_fields) : $term_fields;
			$merged['entry_class_name'] = empty($merged['entry_class_name']) ? $merged['name'] : $merged['entry_class_name'];

			if(
				$hide_inactive && (
					empty($merged['is_active']) || 
					!file_exists($comic['settings']['entry_image_base_path'] . $merged['slug'] . '/') || 
					!file_exists($comic['settings']['entry_image_base_path'] . $merged['slug'] . '_original/')
				)
			)
				continue;

			$key = 'slug' == $key ? 'slug' : 'term_id';
			$fields[$merged[$key]] = $merged;
		}
	}

	return $fields;
}

/**
 * Entry 画面 Entry Class 選択ドロップダウンの option を出力
 */
function sma2013comic_entry_class_select_options() {
	$fields = sma2013comic_get_entry_category_fields();
	$options = '';

	if(empty($fields))
		return '<p>No audition at the moment.</p>';

	if(1 === count($fields)) {
		$field = reset($fields);
		$field['zip_file_required'] = isset($field['zip_file_required']) ? $field['zip_file_required'] : 0;
		$options .= '<option value="' . esc_attr($field['slug']) . '" data-zip-required="' . esc_attr($field['zip_file_required']) . '">' . sanitize_text_field($field['entry_class_name']) . '</option>' . "\n";
	}
	else {
		$options .= '<option value="" selected>Please Select</option>' . "\n";

		foreach ($fields as $field) {
			$field['zip_file_required'] = isset($field['zip_file_required']) ? $field['zip_file_required'] : 0;
			$options .= '<option value="' . esc_attr($field['slug']) . '" data-zip-required="' . esc_attr($field['zip_file_required']) . '">' . sanitize_text_field($field['entry_class_name']) . '</option>' . "\n";
		}
	}

	return '
		<h2>Please select the audition to enter and click \'Next\' to proceed.</h2>
		<p>
			<select name="entry_class" id="entry_class" required>
				' . $options . '
			</select>
		</p>
		<p>
			<button id="entry-class-button" type="button">Next</button>
		</p>
	';
}

/**
 * log を wp_comic_log テーブルに記録する
 */
function sma2013comic_insert_log($action_type = '', $description = '', $object_id = 0, $mailed = '') {
	global $wpdb;
	if(!isset($wpdb->comic_log))
		$wpdb->comic_log = $wpdb->prefix . 'comic_log';

	$user = wp_get_current_user();
	$user_login = isset( $user->user_login ) ? $user->user_login : '';

	$wpdb->insert( $wpdb->comic_log, array(
		'log_date' => date('Y-m-d H:i:s'),
		'user_login' => $user_login,
		'action_type' => $action_type,
		'description' => $description,
		'object_id' => $object_id,
		'mailed' => $mailed,
	));
}

/**
 * ユーザーにエントリーステータスの変更をメールする
 */
function sma2013comic_entry_status_notification($object_id, $term_id, $taxonomy, $result, $updated) {
	if(!$updated || 'status' != $taxonomy || is_wp_error($result))
		return;

	$user_login = get_field('user_login', $object_id);
	$user = get_user_by('login', $user_login);
	$email = $user->user_email;

	if(empty($email) || !is_email($email))
		return;

	$subject = 'Silent Manga Audition : ' . $object_id . ' Change of Status';
	$status = get_term_field( 'name', $term_id, 'status', 'edit');

	$message = 'This is an automated notification mail sent by Silent Manga Audition Manga entry system.' . "\r\n\r\n";
	$message .= 'There has been a change to the following entry.' . "\r\n\r\n";
	$message .= 'ID: ' . $object_id . "\r\n";
	$message .= get_the_title($object_id) . "\r\n\r\n";
	$message .= 'Details: ' . "\r\n";
	$message .= $status . "\r\n\r\n";
	$message .= 'Login to see the details' . "\r\n";
	$message .= 'http://comic.manga-audition.com/login/' . "\r\n\r\n";
	$message .= '# If you require any assistance, please first contact us via your dashboard.' . "\r\n\r\n\r\n";
	$message .= 'Silent Manga Audition Committee' . "\r\n";
	$message .= 'www.manga-audition.com' . "\r\n";

	$result = @wp_mail($email, $subject, $message);

	$mailed = false !== $result ? 'sent' : 'failed';
	sma2013comic_insert_log('entry-status-change', $status, $object_id, $mailed);
}
add_action('sma2013comic_single_value_taxonomy_updated', 'sma2013comic_entry_status_notification', 10, 5);

/**
 * サニタイズした $_POST データを返す（コンテキストに応じて返り値に wp_slash() 等が必要）
 */
function sma2013comic_get_sanitized_post_data() {
	$post = array();

	if(isset($_POST) && is_array($_POST)) {
		foreach ($_POST as $key => $value) {
			$post[$key] = sanitize_text_field(wp_unslash($value));
		}
	}

	return $post;
}

/**
 * 管理者に新規エントリーのメールを送る
 */
function sma2013comic_new_entry_admin_notification($data = array()) {
	extract($data);
	$post = sma2013comic_get_sanitized_post_data();
	$post_id = (int) $post_id;

	if(empty($entry_class['notification_e-mail_address']) || !is_email($entry_class['notification_e-mail_address']))
		return;

	$subject = '[Silent Manga Audition] New Entry Notification: ' . $post_id;

	$message = 'This is an automated notification mail sent by Silent Manga Audition Manga Entry System.' . "\r\n\r\n";

	$message .= '[Entry Class] ' . "\r\n";
	$message .= wp_strip_all_tags($entry_class['entry_class_name'], true);
	$message .= "\r\n\r\n";
	$message .= '[Author Name] ' . "\r\n";
	$message .= wp_strip_all_tags($user['nickname'], true);
	$message .= "\r\n\r\n";
	$message .= '[Country] ' . "\r\n";
	$message .= sma2013comic_get_country_code_info($user['country_code'], 'name');
	$message .= "\r\n\r\n";
	$message .= '[The first page is] ' . "\r\n";
	$message .= isset($post['the_first_page_is']) && '2' == $post['the_first_page_is'] ? 'Double Page Spread' : 'Single Page';
	$message .= "\r\n\r\n";
	$message .= '[Title] ' . "\r\n";
	$message .= isset($post['entry_titile_orig']) ? $post['entry_titile_orig'] : '';
	$message .= "\r\n\r\n";
	$message .= '[Best Part of Manga] ' . "\r\n";
	$message .= isset($post['entry_best_part_of_manga']) ? $post['entry_best_part_of_manga'] : '';
	$message .= "\r\n\r\n";
	$message .= '[Comment] ' . "\r\n";
	$message .= isset($post['entry_comment']) ? $post['entry_comment'] : '';
	$message .= "\r\n\r\n";
	$message .= '[Is this a team effort?] ' . "\r\n";
	$message .= !empty($post['entry_is_this_a_team_effort']) ? 'Yes' : 'No';
	$message .= "\r\n\r\n";
	$message .= '[Credit] ' . "\r\n";
	$message .= isset($post['entry_credit']) ? $post['entry_credit'] : '';
	$message .= "\r\n\r\n";
	$message .= '[User Dashboard] ' . "\r\n";
	$message .= home_url( 'users/' . $user['ID'] . '/' );
	$message .= "\r\n\r\n\r\n";

	$message .= 'To view the manga of this entry, ';
	$message .= 'please login via http://comic.manga-audition.com/login/ and then go to ';
	$message .= 'http://comic.manga-audition.com/?post_type=entries&p=' . $post_id . '&preview=true' . "\r\n";
	$message .= "\r\n\r\n";

	$message .= '--' . "\r\n";
	$message .= 'Silent Manga Audition Manga Entry System' . "\r\n";

	@wp_mail($entry_class['notification_e-mail_address'], $subject, $message);
}

/**
 * 値を一つしか持てないようにタクソノミーを設定する
 */
function sma2013comic_set_single_value_taxonomy($object_id = null, $terms = array(), $tt_ids = array(), $taxonomy = null, $append = null, $old_tt_ids = array()) {
	// Set target taxonomies
	$sv_taxonomies = array('status', 'region', 'entry-category');
	// Check conditions
	if(false === array_search($taxonomy, $sv_taxonomies) || empty($object_id))
		return;
	// get single term to update with
	if(0 !== count($tt_ids) && isset($tt_ids[count($tt_ids) - 1]))
		$term_id = (int) $tt_ids[count($tt_ids) - 1];
	else
		return;

	// Skip if the term count will be 1. This line also prevent looping.
	if(1 === count($tt_ids) && 0 === count($old_tt_ids))
		return;

	// Set false if no term changes
	$updated = true;
	if(1 === count($old_tt_ids) && $term_id === (int) $old_tt_ids[0])
		$updated = false;

	wp_delete_object_term_relationships($object_id, $taxonomy);
	$result = wp_set_object_terms($object_id, $term_id, $taxonomy);

	// $result may contain WP_Error. 
	do_action('sma2013comic_single_value_taxonomy_updated', $object_id, $term_id, $taxonomy, $result, $updated);
}
add_action('set_object_terms', 'sma2013comic_set_single_value_taxonomy', 10, 6);

/**
 * Make New Entry 新規記事作成、画像ファイル移動
 */
function sma2013comic_make_new_entry() {
	global $comic;

	if (!$user = wp_get_current_user())
		return false;

	if (!isset($_POST['fuid']))
		return false;

	$entry_category_fields = sma2013comic_get_entry_category_fields();
	$selected_audition = !empty($_POST['entry_class']) ? sanitize_text_field(wp_unslash($_POST['entry_class'])) : false;

	if(!$selected_audition || !isset($entry_category_fields[$selected_audition]))
		return false;

	$selected_audition_dir = dirname(ABSPATH) . '/images/entries/' . $selected_audition . '/';
	$selected_audition_orig_dir = dirname(ABSPATH) . '/images/entries/' . $selected_audition . '_original/';

	$title = sanitize_text_field(wp_unslash($_POST['entry_titile_orig'])); // wp_slash() this later

	$new_post = array(
		'post_title' => $title . ' by ' . $user->nickname,
		'post_status' => 'draft',
		'post_author' => 1,
		'post_type' => 'entries',
	);
	$new_post = wp_slash($new_post);
	$post_id = wp_insert_post( $new_post );

	wp_set_object_terms($post_id, $selected_audition, 'entry-category');
	wp_set_object_terms($post_id, sanitize_title(sma2013comic_get_country_code_info($user->country_code, 'region')), 'region');

	update_field( 'field_5213a823ef5bb', wp_slash($user->user_login), $post_id ); // user_login, Text
	update_field( 'field_5213a85315ec5', wp_slash($title), $post_id ); // titile, Text
	update_field( 'field_5213a89f15ec6', wp_slash($title), $post_id ); // titile_orig, Text
	update_field( 'field_54f917c636bd0', (int) $_POST['the_first_page_is'], $post_id ); // The first page is, Select
	update_field( 'field_523ec943b931c', wp_slash(sanitize_text_field(wp_unslash($_POST['entry_best_part_of_manga']))), $post_id ); // best_part_of_manga, Text Area 
	update_field( 'field_523ec75ec9ae7', wp_slash(sanitize_text_field(wp_unslash($_POST['entry_comment']))), $post_id ); //comment, Text Area 
	update_field( 'field_523ec08bb345e', !empty($_POST['entry_is_this_a_team_effort']) ? true : false, $post_id ); //is_this_a_team_effort, True / False 
	update_field( 'field_523ec029b345d', wp_slash(sanitize_text_field(wp_unslash($_POST['entry_credit']))), $post_id ); //credit, Text Area 

	// mkdir
	if ( !@mkdir( $selected_audition_dir . $post_id ) )
			return false;
	if ( !@mkdir( $selected_audition_orig_dir . $post_id ) )
			return false;

	if ( ! $dh = opendir($comic['settings']['entry_image_uploads_tmp']) )
		return false;

	$images = array();
	while ( ( $file = readdir( $dh ) ) !== false ) {
		if(preg_match('/.+\.(jpg|png|zip)$/i', $file)) {
			if(false !== strpos($file, $_POST['fuid'] . '_'))
				$images[] = $file;
		}
	}
	closedir( $dh );

	if(0 === count($images))
		return false;

	sort($images);

	$failed_count = 0;
	foreach($images as $num => $image) {
		// Move original files
		$new_orig_file = $selected_audition_orig_dir . $post_id . '/' . sanitize_file_name(str_replace($_POST['fuid'] . '_', '', $image));
		if(!@rename($comic['settings']['entry_image_uploads_tmp'] . '/' . $image, $new_orig_file))
			$failed_count += 1;
	}

	if(0 !== $failed_count)
		return false;

	// seems no problems moving original files so we set this.
	$result = wp_set_object_terms($post_id, 'waiting-verification', 'status');
	$term = get_term_by('slug', 'waiting-verification', 'status');
	if(isset($term->term_id)) {
		// mail to user
		sma2013comic_entry_status_notification($post_id, $term->term_id, 'status', $result, true);
		// mail to admin
		sma2013comic_new_entry_admin_notification(array(
			'user' => array(
				'nickname' => $user->nickname,
				'country_code' => $user->country_code,
				'ID' => $user->ID,
			),
			'post_id' => $post_id,
			'entry_class' => $entry_category_fields[$selected_audition]
		));
	}

	// async convert images
	$src = escapeshellarg(basename($selected_audition_orig_dir) . '/' . $post_id);
	$dst = escapeshellarg(basename($selected_audition_dir) . '/' . $post_id);
	$prefix = escapeshellarg(sma2013comic_sanitize_file_name(sanitize_file_name(sanitize_title($user->nickname))));
	exec($comic['settings']['imgresize_dir'] . 'imgresize -s ' . $src . ' -d ' . $dst . ' -p ' . $prefix . ' > /dev/null 2>&1 &');

	return $post_id;
}

/**
 * borrowed from WPMP
 */
function sma2013comic_sanitize_file_name( $name ) {
	$info = pathinfo( $name );
	$ext = !empty( $info['extension'] ) ? '.' . $info['extension'] : '';
	$name = str_replace( $ext, '', $name );
	$name_enc = rawurlencode( $name );
	$name = ( $name == $name_enc ) ? $name . $ext : md5( $name ) . $ext;
	return $name;
}

/**
 * update か、表示か、で切り分けたユーザープロフィールフォームの値を持つオブジェクトを返す
 */
function sma2013comic_get_users_profile_field_values() {
	global $profileuser;

	$names = array(
		'where_did_you_hear_about_us',
		'description',
		'projects',
		'occupation',
		'date_of_birth',
		'phone_number',
		'postal_address',
		'country_code',
		'nickname',
		'last_name',
		'first_name',
	);

	$update = false;
	if(!empty($_POST['user_id']) && 'profile' == $_POST['action'])
		$update = true;

	$return = new stdClass();

	foreach($names as $name) {
		if($update)
			$return->$name = isset($_POST[$name]) ? sanitize_text_field(wp_unslash($_POST[$name])) : '';
		else
			$return->$name = isset($profileuser->$name) ? $profileuser->$name : '';
	}

	// Super tricky. 'user_email' is 'email' in the form.
	if($update)
		$return->user_email = isset($_POST['email']) ? sanitize_text_field(wp_unslash($_POST['email'])) : '';
	else
		$return->user_email = isset($profileuser->user_email) ? $profileuser->user_email : '';

	return $return;
}

/**
 * ユーザープロフィール更新のエラーチェック（非管理画面）
 */
function sma2013comic_user_profile_update_errors($errors, $update, $user) {
	if(is_admin() || !$update)
		return;

	$empty_error = false;
	$must_fill = array();
	$must_fill[] = $user->first_name;
	$must_fill[] = $user->last_name;
	$must_fill[] = sanitize_text_field($_POST['country_code']);
	$must_fill[] = sanitize_text_field($_POST['postal_address']);
	$must_fill[] = sanitize_text_field($_POST['phone_number']);
	$must_fill[] = sanitize_text_field($_POST['occupation']);
	$must_fill[] = sanitize_text_field($_POST['projects']);
	$must_fill[] = sanitize_text_field($_POST['description']);
	$must_fill[] = sanitize_text_field($_POST['where_did_you_hear_about_us']);

	foreach($must_fill as $must_fill_field)
		if(empty($must_fill_field))
			$empty_error = true;

	if($empty_error)
		$errors->add('empty_must_fill', '<strong>ERROR</strong>: Please fill in all fields.');

	$date_of_birth = sanitize_text_field($_POST['date_of_birth']);
	if(!preg_match('/^(19|20)\d\d\/(0[1-9]|1[012])\/(0[1-9]|[12][0-9]|3[01])$/', $date_of_birth))
		$errors->add('invalid_date_of_birth', '<strong>ERROR</strong>: The date of birth must be in format: yyyy/mm/dd.');

	$country_code = sanitize_text_field($_POST['country_code']);
	if(!empty($country_code) && !preg_match('/^[A-Z][A-Z]$/', $country_code))
		$errors->add('invalid_country_code', '<strong>ERROR</strong>: The country code isn&#8217;t correct.');
}
add_action( 'user_profile_update_errors', 'sma2013comic_user_profile_update_errors', 10, 3);


/**
 * Widget エリア登録
 */
function sma2013comic_widgets_init() {
	register_sidebar( array(
		'name' => 'User Dashboards Sidebar',
		'id' => 'sidebar-4',
		'description' => 'User Dashboards Sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'sma2013comic_widgets_init', 11);

/**
 * ユーザーメニュー
 */
register_nav_menu( 'users-primary', 'Users Primary Menu - logged in' );
register_nav_menu( 'users-primary-outside', 'Users Primary Menu - logged out' );

/**
 * /%key=value%/ 形式記述で wp_nav_menu のカスタムに URL を出力
 */
function sma2013comic_wp_nav_menu_replace( $nav_menu = '' ) {
	if(false !== strpos($nav_menu, '%user_home=') && false === strpos($nav_menu, 'current-menu-item'))
		$nav_menu = str_replace('menu-item-type-custom', 'menu-item-type-custom current-menu-item', $nav_menu);
	return preg_replace_callback('|"/%([^/%="]+)\=([^/%="]+)%/"|', 'sma2013comic_wp_nav_menu_replace_cb', $nav_menu);
}
add_filter( 'wp_nav_menu', 'sma2013comic_wp_nav_menu_replace');

function sma2013comic_wp_nav_menu_replace_cb( $matches ) {
	if('user_home' == $matches[1]) {
		$user = wp_get_current_user();
		$user_id = isset( $user->ID ) ? $user->ID : '';
		return '"' . home_url( 'users/' . $user_id . '/' ) . '"';
	}
	else
		return $matches[0];
}

/**
 * 現在の記事のスラッグを返す
 */
function sma2013comic_get_cerrent_post_slug() {
	$queried_object = get_queried_object();
	return isset($queried_object->post_name) ? $queried_object->post_name : '';
}

/**
 * Add author profile meta box to entries
 */
function sma2013comic_author_profile_add_meta_box() {
	add_meta_box(
		'author-profile-link',
		'Author Profile',
		'sma2013comic_author_profile_meta_box_callback',
		'entries',
		'side',
		'core'
	);
}
add_action( 'add_meta_boxes', 'sma2013comic_author_profile_add_meta_box' );

/**
 * Print author profile box content
 */
function sma2013comic_author_profile_meta_box_callback( $post ) {
	$user_login = get_field('user_login', $post);
	$user = get_user_by('login', $user_login);
	if(isset($user->ID))
		echo '<a href="' . esc_url(admin_url() . '/user-edit.php?user_id=' . $user->ID) . '">' . esc_html($user_login) . '</a>';
	else
		echo 'User not found';
}

/**
 * ユーザーがユーザーホームを見る権限があるか確認する
 */
function sma2013comic_check_user_can_view_users_home() {
	if (!is_user_logged_in())
		return false;

	$user = wp_get_current_user();
	$user_id = isset( $user->ID ) ? $user->ID : false;
	$post_name = sma2013comic_get_cerrent_post_slug();
	if(!is_super_admin($user_id) && !current_user_can('extended_editor') && $post_name != $user_id)
		return false;

	return true;
}

/**
 * ユーザーがフォームを完全に入力しているか確認する
 */
function sma2013comic_check_user_completed_profile_form() {
	if (!is_user_logged_in())
		return false;

	$user = wp_get_current_user();
	$user_id = isset( $user->ID ) ? $user->ID : false;
	$profileuser = get_userdata( $user_id );

	$names = array(
		'where_did_you_hear_about_us',
		'description',
		'projects',
		'occupation',
		'date_of_birth',
		'phone_number',
		'postal_address',
		'country_code',
		'nickname',
		'last_name',
		'first_name',
		'user_email',
	);

	$completed = true;

	foreach($names as $name) {
		if(empty($profileuser->$name))
			$completed = false;
	}

	return $completed;
}

/**
 * ユーザーログイン時にユーザーホームへリダイレクトする
 */
function sma2013comic_login_redirect( $redirect_to = '',  $request_redirect_to = '', $user = '') {
	if(isset($user->ID) && !user_can($user->ID, 'edit_others_posts')) {
		// If the user has no his page, create one.
		global $wpdb;
		$post_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $user->ID, 'users'));
		if(empty($post_id))
			sma2013comic_create_user_page($user->ID);

		return home_url( 'users/' . $user->ID . '/' );
	}
	else
		return $redirect_to;
}
add_filter( 'login_redirect', 'sma2013comic_login_redirect', 10, 3);

/**
 * admin 以外で admin bar を無効にする
 */
function sma2013comic_remove_admin_bar() {
	if (!is_super_admin() && !is_admin()) {
		show_admin_bar(false);
	}
}
add_action('after_setup_theme', 'sma2013comic_remove_admin_bar');


/**
 * twentytwelve_comment をオーバーライド
 */
function twentytwelve_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'twentytwelve' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<header class="comment-meta comment-author vcard">
				<?php
					echo get_avatar( $comment, 44 );
					printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( is_super_admin($comment->user_id) ) ? '<span>Staff</span>' : ''
					);
					printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						/* translators: 1: date, 2: time */
						sprintf( __( '%1$s at %2$s', 'twentytwelve' ), get_comment_date(), get_comment_time() )
					);
				?>
			</header><!-- .comment-meta -->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentytwelve' ); ?></p>
			<?php endif; ?>

			<section class="comment-content comment">
				<?php comment_text(); ?>
				<?php edit_comment_link( __( 'Edit', 'twentytwelve' ), '<p class="edit-link">', '</p>' ); ?>
			</section><!-- .comment-content -->

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'twentytwelve' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}

/**
 * 新規ユーザーにユーザー連絡用コメントページを作成する
 */
function sma2013comic_create_user_page($user_id) {
	$user = get_user_by( 'id', $user_id );
	$new_post = array(
		'post_title' => $user->user_login,
		'post_status' => 'publish',
		'post_author' => 1,
		'post_type' => 'users',
		'post_name' => $user_id,
		'comment_status' => 'open',
	);
	$new_post = wp_slash($new_post);
	$post_id = wp_insert_post( $new_post );
}
//add_action('user_register', 'sma2013comic_create_user_page');

function sma2013comic_entry_date() {
	$output = '<span class="entry-date">' . sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>' . '</span>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( strtoupper(get_the_date('d F Y')) )
	) . '</span>';
	$output .= '<span class="category-list">' . get_the_category_list(', ') . '<span class="arrow-up"></span></span>';
	return $output;
}

function sma2013comic_output_post_thumbnail_html() {
	$posts = get_posts('meta_key=_thumbnail_id&meta_compare=>=&meta_value=1&numberposts=12');
	$output = '';
		foreach ($posts as $post) {
			$thumbnail = get_the_post_thumbnail($post->ID, 'thumbnail');
			$thumbnail = str_replace('width="150"', 'width="76"', $thumbnail);
			$thumbnail = str_replace('height="150"', 'height="76"', $thumbnail);
			$output .= '<a href="' . esc_url(get_permalink($post->ID)) . '" title="' . esc_attr( $post->post_title ) . '" >' . $thumbnail . '</a>';
		}
	return $output;
}

/**
 * 関数の返り値を出力するウィジェット
 */
class sma2013comic_Content_Widget extends WP_Widget {
	function __construct() {
		$widget_ops = array('classname' => 'widget_sma2013comic_content', 'description' => 'Do Functions');
		parent::__construct('sma2013comic_content', 'Do Functions', $widget_ops);
	}

	function widget($args, $instance) {
		extract($args);
		$source = $instance['source'];
		$content = '';

		if(empty($source))
			return;

		if(false !== stripos($source, 'sma2013') && is_callable($source))
			$content = call_user_func($source);

		if(empty($content))
			return;

		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
		echo $content;
		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['source'] = strip_tags($new_instance['source']);
		return $instance;
	}

	function form($instance) {
		$instance = wp_parse_args((array) $instance, array('title' => '', 'source' => ''));
		$title = strip_tags($instance['title']);
		$source = strip_tags($instance['source']);
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php echo 'Title'; ?></label><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('source'); ?>"><?php echo 'Function'; ?></label><input class="widefat" id="<?php echo $this->get_field_id('source'); ?>" name="<?php echo $this->get_field_name('source'); ?>" type="text" value="<?php echo esc_attr($source); ?>" /></p>
<?php
	}
}
add_action( 'widgets_init', create_function('', 'register_widget("sma2013comic_Content_Widget");'));


/**
 * マップフィルター HTML 返す
 */
function sma2013comic_map_filter_html() {
	if(!is_front_page())
		return false;
	return '
<div id="marker-filter">
<ul>
<li class="africa"><div class="circle"></div><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"AFRICA"}\' checked><a href="#" class="zoom" data-id="AFRICA">AFRICA</a></li>
<li class="asia"><div class="circle"></div><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"ASIA"}\' checked><a href="#" class="zoom" data-id="ASIA">ASIA</a></li>
<li class="europe"><div class="circle"></div><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"EUROPE"}\' checked><a href="#" class="zoom" data-id="EUROPE">EUROPE</a></li>
<li class="middle-east"><div class="circle"></div><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"MIDDLE EAST"}\' checked><a href="#" class="zoom" data-id="MIDDLE EAST">MIDDLE EAST</a></li>
<li class="north-america"><div class="circle"></div><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"NORTH AMERICA"}\' checked><a href="#" class="zoom" data-id="NORTH AMERICA">NORTH AMERICA</a></li>
<li class="oceania"><div class="circle"></div><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"OCEANIA"}\' checked><a href="#" class="zoom" data-id="OCEANIA">OCEANIA</a></li>
<li class="south-america"><div class="circle"></div><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"SOUTH AMERICA"}\' checked><a href="#" class="zoom" data-id="SOUTH AMERICA">SOUTH AMERICA</a></li>
<li class="orphaned"><div class="circle"></div><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"ORPHANED"}\' checked><a href="#" class="zoom" data-id="ORPHANED">Orphaned Works</a></li>
</ul>
</div>
';
}

/**
 * マップフィルター euro_asia HTML 返す
 */
function sma2013comic_map_filter_html_euro_asia() {
	return '
<div id="marker-filter">
<ul>
<li><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"AFRICA"}\' checked><a href="#" class="zoom" data-id="AFRICA">AFRICA</a></li>
<li><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"ASIA"}\' checked><a href="/" data-id="ASIA"><strong>ASIA</strong></a></li>
<li><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"EUROPE"}\' checked><a href="/europe/" data-id="EUROPE"><strong>EUROPE</strong></a></li>
<li><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"MIDDLE EAST"}\' checked><a href="#" class="zoom" data-id="MIDDLE EAST">MIDDLE EAST</a></li>
<li><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"NORTH AMERICA"}\' checked><a href="#" class="zoom" data-id="NORTH AMERICA">NORTH AMERICA</a></li>
<li><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"OCEANIA"}\' checked><a href="#" class="zoom" data-id="OCEANIA">OCEANIA</a></li>
<li><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"SOUTH AMERICA"}\' checked><a href="#" class="zoom" data-id="SOUTH AMERICA">SOUTH AMERICA</a></li>
<li><input type="checkbox" class="marker-group" data-mkgp=\'{"type":"region", "id":"ORPHANED"}\' checked><a href="#" class="zoom" data-id="ORPHANED">Orphaned Works</a></li>
</ul>
</div>
';
}

/**
 * カタログエントリーデータ json 書出し
 */
function sma2013comic_generate_wp_entries_json() {
	global $post;
	//the format is {"code":"JP","year":"2012","cat":"comedy"}
	//the format is {"id":"P000920","title":"xxx","author":"yyy" ,"code":"JP","year":"2012","cat":"comedy"}
	$all = array();
	$posts = get_posts(array('post_type' => 'entries', 'order' => 'ASC', 'numberposts' => -1));

	foreach ($posts as $post) {
		setup_postdata($post);
		$array = array();

		// year
		$array['year'] = '';
		$post_terms = wp_get_object_terms($post->ID, 'entry-category', array('fields' => 'slugs'));
		if(is_array($post_terms) && count($post_terms) === 1)
			$array['year'] =  str_replace('zma', '', $post_terms[0]);
		// id
		if('2013' == $array['year'])
			$array['id'] = get_field('entry_id', $post->ID);
		else
			$array['id'] = (string) $post->ID;
		// slug
		$array['slug'] = $post->post_name;
		// title
		$array['title'] = sma2013comic_get_title();
		// author
		$array['author'] = sma2013comic_get_penname();
		// code
		$array['code'] = sma2013comic_get_country_code();
		// cat
		$array['cat'] = '';
		$post_terms = wp_get_object_terms($post->ID, 'manga_style', array('fields' => 'slugs'));
		if(is_array($post_terms) && count($post_terms) === 1)
			$array['cat'] = $post_terms[0];

		$all[] = $array;
	}
	wp_reset_postdata();

	file_put_contents(dirname( __FILE__ ) . '/js/wp-entries.json' , 'var wp_entries = ' . json_encode($all) . ';');
}
add_action( 'sma2013comic_generate_wp_entries', 'sma2013comic_generate_wp_entries_json' );

/**
 * setup schedule
 */
function sma2013comic_prefix_setup_schedule() {
	if ( ! wp_next_scheduled( 'sma2013comic_generate_wp_entries' ) )
		wp_schedule_event( time(), 'twicedaily', 'sma2013comic_generate_wp_entries');
}
add_action( 'wp', 'sma2013comic_prefix_setup_schedule' );

/**
 * エントリー画像の URL を配列で返すショートコード
 */
function sma2013comic_get_manga_images_shortcode($atts) {
	extract(shortcode_atts(array(
		'dir_name' => 'zma2013',
	), $atts));
	return sma2013comic_get_entry_meta() . sma2013comic_get_entry_images($dir_name);
}
add_shortcode('manga_images', 'sma2013comic_get_manga_images_shortcode');


/**
 * pre_get_posts 
 */
function sma2013comic_main_query_customizer( $query ) {
	if( is_feed() )
		wp_die( 'No feeds available' );

	if( !is_admin() && is_archive() ) {
		$query->set_404();
		status_header(404);
	}

	if( !is_admin() && is_tax('status') ) {
		$query->set_404();
		status_header(404);
	}
}
add_action( 'pre_get_posts', 'sma2013comic_main_query_customizer', 1 );

/**
 * エントリーオーサーのニックネームを返す
 */
function sma2013comic_get_penname() {
	$user_login = get_field('user_login', get_the_ID());
	$user = get_user_by('login', $user_login);
	$nickname = get_user_meta($user->ID,  'nickname', true);
	return isset($nickname) ? $nickname : $user_login;
}

/**
 * エントリーオーサーの国コードを返す
 */
function sma2013comic_get_country_code() {
	$user_login = get_field('user_login', get_the_ID());
	$user = get_user_by('login', $user_login);
	$code = get_user_meta($user->ID,  'country_code', true);
	return isset($code) ? $code : '';
}

/**
 * エントリーのタイトルを返す
 */
function sma2013comic_get_title() {
	return @implode('', @explode(' by ', get_the_title(), -1));
}

/**
 * エントリーのメタ情報の HTML を返す
 */
function sma2013comic_get_entry_meta() {
	sma2013comic_get_penname();
	$output = '<ul class="entry-meta">' . "\n";
	$output .= '<li>TITLE: ' . sma2013comic_get_title() . '</li>' . "\n";
	$output .= '<li>AUTHOR: ' . sma2013comic_get_penname() . '</li>' . "\n";
	$output .= '<li>COUNTRY: ' . sma2013comic_get_country_code_info(sma2013comic_get_country_code(), 'name') . '</li>' . "\n";
	if($best_part_of_manga = get_field('best_part_of_manga'))
		$output .= '<li>BEST PART OF MANGA: ' . esc_html($best_part_of_manga) . '</li>' . "\n";
	if($comment = get_field('comment'))
		$output .= '<li>COMMENT: ' . esc_html($comment) . '</li>' . "\n";
	$output .= '</ul>' . "\n";
	return $output;
}

/**
 * エントリー画像の img タグを返す
 */
function sma2013comic_get_entry_images($post_id = null, $return_urls = false) {
	global $comic, $sma2013comic_cache;

	if(!$post_id)
		return false;

	if(isset($sma2013comic_cache['entry_image_urls_' . $post_id]) && is_array($sma2013comic_cache['entry_image_urls_' . $post_id])) {
		$urls = $sma2013comic_cache['entry_image_urls_' . $post_id];
	}
	else {
		$entry_category = wp_get_object_terms($post_id, 'entry-category', array('fields' => 'slugs'));
		if(is_array($entry_category) && count($entry_category) === 1)
			$entry_category = $entry_category[0];
		else
			$entry_category = 'unknown';

		if(empty($entry_category))
			$entry_category = 'zma2013';

		if($entry_category == 'zma2013') {
			$entry_id = get_field('entry_id');
			if(empty($entry_id))
				return false;

			$dir = $comic['settings']['entry_image_base_path'] . $entry_category . '/' . substr($entry_id, 3) . '/';
			$dir_url = $comic['settings']['entry_image_base_url'] . $entry_category . '/' . substr($entry_id, 3) . '/';
		}
		else {
			$dir = $comic['settings']['entry_image_base_path'] . $entry_category . '/' . (int) $post_id . '/';
			$dir_url = $comic['settings']['entry_image_base_url'] . $entry_category . '/' . (int) $post_id . '/';
		}

		$urls = array();

		if ( !is_dir($dir) ) {
			return false;
		}
		if ( ! $dh = opendir($dir) ) {
			return false;
		}
		while ( ( $image = readdir( $dh ) ) !== false ) {
			if(false !== strpos($image, '.jpg') || false !== strpos($image, '.png'))
				$urls[] = $dir_url . $image;
		}
		closedir( $dh );
		sort($urls);
		$sma2013comic_cache['entry_image_urls_' . $post_id] = $urls;
	}

	if($return_urls)
		return $urls;

	$output = '';

	$num = count($urls);
	foreach ($urls as $url) {
		$num -= 1;
		if(0 !== $num)
			$output .= '<img src="' . esc_url($url) . '"><hr>' . "\n";
		else
			$output .= '<img src="' . esc_url($url) . '">' . "\n";
	}
	return $output;
}

/**
 * エントリーページに twitter card の meta タグを出力する
 */
function sma2013comic_single_entries_twitter_card() {
	global $wp_query;

	$post_id = (int) $wp_query->get_queried_object_id();

	if(!is_singular('entries') || empty($post_id))
		return;

	$title = esc_attr(single_post_title('', false));

	$description = '';
	if($comment = get_field('comment'))
		$description .= esc_attr($comment);
	if($best_part_of_manga = get_field('best_part_of_manga'))
		$description .= ' BEST PART OF MANGA: ' . esc_attr($best_part_of_manga);

	if(empty($description))
		$description .= $title;

	$images = sma2013comic_get_entry_images($post_id, true);
	$image = !empty($images[0]) ? esc_attr($images[0]) : '';

	echo '
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@SilentMangaComm">
<meta name="twitter:creator" content="@SilentMangaComm">
<meta name="twitter:title" content="' . $title . '">
<meta name="twitter:description" content="' . $description . '">
<meta name="twitter:image" content="' . $image . '">
';
}
//add_action('wp_head','sma2013comic_single_entries_twitter_card');

/**
 * エントリー画像のリンク a タグを数字で返す（ユーザーダッシュボードステータス用）
 */
function sma2013comic_get_entry_image_links($post_id = null) {
	if(!$post_id)
		return 'n/a';

	$urls = sma2013comic_get_entry_images($post_id, true);
	if(!is_array($urls))
		return 'n/a';

	$output = '';
	foreach ($urls as $key => $url) {
		$output .= '<a href="' . esc_url($url) . '" class="entry-file" rel="eid-' . $post_id . '">' . ($key + 1) . "</a>\n";
	}
	return $output;
}

/**
 * エントリー画像のサムネールの img タグを返す
 */
function sma2013comic_get_entry_tn_image($dir_name = 'zma2013') {
	global $comic;
	$entry_id = get_field('entry_id');
	if(empty($entry_id))
		return false;

	$tn_url = $comic['settings']['entry_image_base_url'] . $dir_name . '/tn/thumb_sma' . substr($entry_id, 3) . '.jpg';
	return  '<img src="' . esc_url($tn_url) . '">' . "\n";
}

/**
 * Add extra fields to user contact
 */
function sma2013comic_user_contactmethods($contactmethods, $user = null) {
	if(isset($user->ID) && is_super_admin( $user->ID ))
		return $contactmethods;

	$contactmethods['country_code'] = 'Country';
	$contactmethods['postal_address'] = 'Postal Address';
	$contactmethods['phone_number'] = 'Phone Number';
	$contactmethods['date_of_birth'] = 'Date of Birth';
	$contactmethods['occupation'] = 'Occupation';
	$contactmethods['projects'] = 'Projects';
	$contactmethods['where_did_you_hear_about_us'] = 'Where did you hear about us? / Any other information regarding you.';
	return $contactmethods;
}
add_filter('user_contactmethods', 'sma2013comic_user_contactmethods', 10, 2);


/**
 * ポストタイプ設定と登録
 */
function sma2013comic_register_post_types() {
	register_post_type('entries', array(
		'label' => 'Entries',
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'has_archive' => false,
		'supports' => array(
			'title',
			//'editor',
			//'excerpt',
			'revisions',
			//'thumbnail',
			//'author',
			//'page-attributes',
			//'comments',
		),
	));

	register_post_type('users', array(
		'label' => 'User Dashboards',
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'has_archive' => false,
		'supports' => array(
			'title',
			//'editor',
			//'excerpt',
			//'revisions',
			//'thumbnail',
			//'author',
			//'page-attributes',
			'comments',
		),
	));
}
add_action('init', 'sma2013comic_register_post_types');

/**
 * タクソノミー設定と登録
 */
function sma2013comic_register_taxonomies() {
	register_taxonomy('status', 'entries', array(
		'label' => 'Entry Status',
		'show_admin_column' => true,
		'show_in_nav_menus' => false,
		'rewrite' => false,
	));

	register_taxonomy('entry-category', 'entries', array(
		'label' => 'Entry Category',
		'show_admin_column' => true,
		'show_in_nav_menus' => false,
	));

	register_taxonomy('region', 'entries', array(
		'label' => 'Region',
		'show_admin_column' => true,
		'show_in_nav_menus' => false,
	));

	register_taxonomy('manga_style', 'entries', array(
		'label' => 'Manga Style',
		'show_admin_column' => false,
		'show_in_nav_menus' => false,
	));
}
add_action('init', 'sma2013comic_register_taxonomies', 0);

/**
 * カントリーコードをキーにして情報を返す
 */
function sma2013comic_get_country_code_info($code, $info, $return_array = false) {
	$array = array (
		'' => 
		array (
			'code' => 'ORPHANED',
			'lat' => '-57.679094',
			'lon' => '-138.515625',
			'name' => 'Orphaned',
			'region' => 'ORPHANED',
		),
		'ORPHANED' => 
		array (
			'code' => 'ORPHANED',
			'lat' => '-57.679094',
			'lon' => '-138.515625',
			'name' => 'Orphaned',
			'region' => 'ORPHANED',
		),
		'AD' => 
		array (
			'code' => 'AD',
			'lat' => '42.5000',
			'lon' => '1.5000',
			'name' => 'Andorra',
			'region' => 'EUROPE',
		),
		'AE' => 
		array (
			'code' => 'AE',
			'lat' => '24.0000',
			'lon' => '54.0000',
			'name' => 'United Arab Emirates',
			'region' => 'MIDDLE EAST',
		),
		'AF' => 
		array (
			'code' => 'AF',
			'lat' => '33.0000',
			'lon' => '65.0000',
			'name' => 'Afghanistan',
			'region' => 'ASIA',
		),
		'AG' => 
		array (
			'code' => 'AG',
			'lat' => '17.0500',
			'lon' => '-61.8000',
			'name' => 'Antigua and Barbuda',
			'region' => 'NORTH AMERICA',
		),
		'AI' => 
		array (
			'code' => 'AI',
			'lat' => '18.2500',
			'lon' => '-63.1667',
			'name' => 'Anguilla',
			'region' => 'NORTH AMERICA',
		),
		'AL' => 
		array (
			'code' => 'AL',
			'lat' => '41.0000',
			'lon' => '20.0000',
			'name' => 'Albania',
			'region' => 'EUROPE',
		),
		'AM' => 
		array (
			'code' => 'AM',
			'lat' => '40.0000',
			'lon' => '45.0000',
			'name' => 'Armenia',
			'region' => 'MIDDLE EAST',
		),
		'AO' => 
		array (
			'code' => 'AO',
			'lat' => '-12.5000',
			'lon' => '18.5000',
			'name' => 'Angola',
			'region' => 'AFRICA',
		),
		'AR' => 
		array (
			'code' => 'AR',
			'lat' => '-34.0000',
			'lon' => '-64.0000',
			'name' => 'Argentina',
			'region' => 'SOUTH AMERICA',
		),
		'AS' => 
		array (
			'code' => 'AS',
			'lat' => '-14.3333',
			'lon' => '-170.0000',
			'name' => 'American Samoa',
			'region' => 'OCEANIA',
		),
		'AT' => 
		array (
			'code' => 'AT',
			'lat' => '47.3333',
			'lon' => '13.3333',
			'name' => 'Austria',
			'region' => 'EUROPE',
		),
		'AU' => 
		array (
			'code' => 'AU',
			'lat' => '-27.0000',
			'lon' => '133.0000',
			'name' => 'Australia',
			'region' => 'OCEANIA',
		),
		'AW' => 
		array (
			'code' => 'AW',
			'lat' => '12.5000',
			'lon' => '-69.9667',
			'name' => 'Aruba',
			'region' => 'NORTH AMERICA',
		),
		'AZ' => 
		array (
			'code' => 'AZ',
			'lat' => '40.5000',
			'lon' => '47.5000',
			'name' => 'Azerbaijan',
			'region' => 'MIDDLE EAST',
		),
		'BA' => 
		array (
			'code' => 'BA',
			'lat' => '44.0000',
			'lon' => '18.0000',
			'name' => 'Bosnia and Herzegovina',
			'region' => 'EUROPE',
		),
		'BB' => 
		array (
			'code' => 'BB',
			'lat' => '13.1667',
			'lon' => '-59.5333',
			'name' => 'Barbados',
			'region' => 'NORTH AMERICA',
		),
		'BD' => 
		array (
			'code' => 'BD',
			'lat' => '24.0000',
			'lon' => '90.0000',
			'name' => 'Bangladesh',
			'region' => 'ASIA',
		),
		'BE' => 
		array (
			'code' => 'BE',
			'lat' => '50.8333',
			'lon' => '4.0000',
			'name' => 'Belgium',
			'region' => 'EUROPE',
		),
		'BF' => 
		array (
			'code' => 'BF',
			'lat' => '13.0000',
			'lon' => '-2.0000',
			'name' => 'Burkina Faso',
			'region' => 'AFRICA',
		),
		'BG' => 
		array (
			'code' => 'BG',
			'lat' => '43.0000',
			'lon' => '25.0000',
			'name' => 'Bulgaria',
			'region' => 'EUROPE',
		),
		'BH' => 
		array (
			'code' => 'BH',
			'lat' => '26.0000',
			'lon' => '50.5500',
			'name' => 'Bahrain',
			'region' => 'MIDDLE EAST',
		),
		'BI' => 
		array (
			'code' => 'BI',
			'lat' => '-3.5000',
			'lon' => '30.0000',
			'name' => 'Burundi',
			'region' => 'AFRICA',
		),
		'BJ' => 
		array (
			'code' => 'BJ',
			'lat' => '9.5000',
			'lon' => '2.2500',
			'name' => 'Benin',
			'region' => 'AFRICA',
		),
		'BM' => 
		array (
			'code' => 'BM',
			'lat' => '32.3333',
			'lon' => '-64.7500',
			'name' => 'Bermuda',
			'region' => 'NORTH AMERICA',
		),
		'BN' => 
		array (
			'code' => 'BN',
			'lat' => '4.5000',
			'lon' => '114.6667',
			'name' => 'Brunei',
			'region' => 'ASIA',
		),
		'BO' => 
		array (
			'code' => 'BO',
			'lat' => '-17.0000',
			'lon' => '-65.0000',
			'name' => 'Bolivia',
			'region' => 'SOUTH AMERICA',
		),
		'BR' => 
		array (
			'code' => 'BR',
			'lat' => '-10.0000',
			'lon' => '-55.0000',
			'name' => 'Brazil',
			'region' => 'SOUTH AMERICA',
		),
		'BS' => 
		array (
			'code' => 'BS',
			'lat' => '24.2500',
			'lon' => '-76.0000',
			'name' => 'Bahamas',
			'region' => 'NORTH AMERICA',
		),
		'BT' => 
		array (
			'code' => 'BT',
			'lat' => '27.5000',
			'lon' => '90.5000',
			'name' => 'Bhutan',
			'region' => 'ASIA',
		),
		'BW' => 
		array (
			'code' => 'BW',
			'lat' => '-22.0000',
			'lon' => '24.0000',
			'name' => 'Botswana',
			'region' => 'AFRICA',
		),
		'BY' => 
		array (
			'code' => 'BY',
			'lat' => '53.0000',
			'lon' => '28.0000',
			'name' => 'Belarus',
			'region' => 'EUROPE',
		),
		'BZ' => 
		array (
			'code' => 'BZ',
			'lat' => '17.2500',
			'lon' => '-88.7500',
			'name' => 'Belize',
			'region' => 'NORTH AMERICA',
		),
		'CA' => 
		array (
			'code' => 'CA',
			'lat' => '60.0000',
			'lon' => '-95.0000',
			'name' => 'Canada',
			'region' => 'NORTH AMERICA',
		),
		'CC' => 
		array (
			'code' => 'CC',
			'lat' => '-12.5000',
			'lon' => '96.8333',
			'name' => 'Cocos (Keeling) Islands',
			'region' => 'ASIA',
		),
		'CD' => 
		array (
			'code' => 'CD',
			'lat' => '0.0000',
			'lon' => '25.0000',
			'name' => 'Congo (Kinshasa)',
			'region' => 'AFRICA',
		),
		'CF' => 
		array (
			'code' => 'CF',
			'lat' => '7.0000',
			'lon' => '21.0000',
			'name' => 'Central African Republic',
			'region' => 'AFRICA',
		),
		'CG' => 
		array (
			'code' => 'CG',
			'lat' => '-1.0000',
			'lon' => '15.0000',
			'name' => 'Congo (Brazzaville)',
			'region' => 'AFRICA',
		),
		'CH' => 
		array (
			'code' => 'CH',
			'lat' => '47.0000',
			'lon' => '8.0000',
			'name' => 'Switzerland',
			'region' => 'EUROPE',
		),
		'CI' => 
		array (
			'code' => 'CI',
			'lat' => '8.0000',
			'lon' => '-5.0000',
			'name' => 'Côte d\'Ivoire',
			'region' => 'AFRICA',
		),
		'CK' => 
		array (
			'code' => 'CK',
			'lat' => '-21.2333',
			'lon' => '-159.7667',
			'name' => 'Cook Islands',
			'region' => 'OCEANIA',
		),
		'CL' => 
		array (
			'code' => 'CL',
			'lat' => '-30.0000',
			'lon' => '-71.0000',
			'name' => 'Chile',
			'region' => 'SOUTH AMERICA',
		),
		'CM' => 
		array (
			'code' => 'CM',
			'lat' => '6.0000',
			'lon' => '12.0000',
			'name' => 'Cameroon',
			'region' => 'AFRICA',
		),
		'CN' => 
		array (
			'code' => 'CN',
			'lat' => '35.0000',
			'lon' => '105.0000',
			'name' => 'China',
			'region' => 'ASIA',
		),
		'CO' => 
		array (
			'code' => 'CO',
			'lat' => '4.0000',
			'lon' => '-72.0000',
			'name' => 'Colombia',
			'region' => 'SOUTH AMERICA',
		),
		'CR' => 
		array (
			'code' => 'CR',
			'lat' => '10.0000',
			'lon' => '-84.0000',
			'name' => 'Costa Rica',
			'region' => 'NORTH AMERICA',
		),
		'CU' => 
		array (
			'code' => 'CU',
			'lat' => '21.5000',
			'lon' => '-80.0000',
			'name' => 'Cuba',
			'region' => 'NORTH AMERICA',
		),
		'CV' => 
		array (
			'code' => 'CV',
			'lat' => '16.0000',
			'lon' => '-24.0000',
			'name' => 'Cape Verde',
			'region' => 'AFRICA',
		),
		'CX' => 
		array (
			'code' => 'CX',
			'lat' => '-10.5000',
			'lon' => '105.6667',
			'name' => 'Christmas Island',
			'region' => 'ASIA',
		),
		'CY' => 
		array (
			'code' => 'CY',
			'lat' => '35.0000',
			'lon' => '33.0000',
			'name' => 'Cyprus',
			'region' => 'MIDDLE EAST',
		),
		'CZ' => 
		array (
			'code' => 'CZ',
			'lat' => '49.7500',
			'lon' => '15.5000',
			'name' => 'Czech Republic',
			'region' => 'EUROPE',
		),
		'DE' => 
		array (
			'code' => 'DE',
			'lat' => '51.0000',
			'lon' => '9.0000',
			'name' => 'Germany',
			'region' => 'EUROPE',
		),
		'DJ' => 
		array (
			'code' => 'DJ',
			'lat' => '11.5000',
			'lon' => '43.0000',
			'name' => 'Djibouti',
			'region' => 'AFRICA',
		),
		'DK' => 
		array (
			'code' => 'DK',
			'lat' => '56.0000',
			'lon' => '10.0000',
			'name' => 'Denmark',
			'region' => 'EUROPE',
		),
		'DM' => 
		array (
			'code' => 'DM',
			'lat' => '15.4167',
			'lon' => '-61.3333',
			'name' => 'Dominica',
			'region' => 'NORTH AMERICA',
		),
		'DO' => 
		array (
			'code' => 'DO',
			'lat' => '19.0000',
			'lon' => '-70.6667',
			'name' => 'Dominican Republic',
			'region' => 'NORTH AMERICA',
		),
		'DZ' => 
		array (
			'code' => 'DZ',
			'lat' => '28.0000',
			'lon' => '3.0000',
			'name' => 'Algeria',
			'region' => 'AFRICA',
		),
		'EC' => 
		array (
			'code' => 'EC',
			'lat' => '-2.0000',
			'lon' => '-77.5000',
			'name' => 'Ecuador',
			'region' => 'SOUTH AMERICA',
		),
		'EE' => 
		array (
			'code' => 'EE',
			'lat' => '59.0000',
			'lon' => '26.0000',
			'name' => 'Estonia',
			'region' => 'EUROPE',
		),
		'EG' => 
		array (
			'code' => 'EG',
			'lat' => '27.0000',
			'lon' => '30.0000',
			'name' => 'Egypt',
			'region' => 'AFRICA',
		),
		'EH' => 
		array (
			'code' => 'EH',
			'lat' => '24.5000',
			'lon' => '-13.0000',
			'name' => 'Western Sahara',
			'region' => 'AFRICA',
		),
		'ER' => 
		array (
			'code' => 'ER',
			'lat' => '15.0000',
			'lon' => '39.0000',
			'name' => 'Eritrea',
			'region' => 'AFRICA',
		),
		'ES' => 
		array (
			'code' => 'ES',
			'lat' => '40.0000',
			'lon' => '-4.0000',
			'name' => 'Spain',
			'region' => 'EUROPE',
		),
		'ET' => 
		array (
			'code' => 'ET',
			'lat' => '8.0000',
			'lon' => '38.0000',
			'name' => 'Ethiopia',
			'region' => 'AFRICA',
		),
		'FI' => 
		array (
			'code' => 'FI',
			'lat' => '64.0000',
			'lon' => '26.0000',
			'name' => 'Finland',
			'region' => 'EUROPE',
		),
		'FJ' => 
		array (
			'code' => 'FJ',
			'lat' => '-18.0000',
			'lon' => '175.0000',
			'name' => 'Fiji',
			'region' => 'OCEANIA',
		),
		'FK' => 
		array (
			'code' => 'FK',
			'lat' => '-51.7500',
			'lon' => '-59.0000',
			'name' => 'Falkland Islands',
			'region' => 'SOUTH AMERICA',
		),
		'FM' => 
		array (
			'code' => 'FM',
			'lat' => '6.9167',
			'lon' => '158.2500',
			'name' => 'Micronesia',
			'region' => 'OCEANIA',
		),
		'FO' => 
		array (
			'code' => 'FO',
			'lat' => '62.0000',
			'lon' => '-7.0000',
			'name' => 'Faroe Islands',
			'region' => 'EUROPE',
		),
		'FR' => 
		array (
			'code' => 'FR',
			'lat' => '46.0000',
			'lon' => '2.0000',
			'name' => 'France',
			'region' => 'EUROPE',
		),
		'GA' => 
		array (
			'code' => 'GA',
			'lat' => '-1.0000',
			'lon' => '11.7500',
			'name' => 'Gabon',
			'region' => 'AFRICA',
		),
		'GB' => 
		array (
			'code' => 'GB',
			'lat' => '54.0000',
			'lon' => '-2.0000',
			'name' => 'United Kingdom',
			'region' => 'EUROPE',
		),
		'GD' => 
		array (
			'code' => 'GD',
			'lat' => '12.1167',
			'lon' => '-61.6667',
			'name' => 'Grenada',
			'region' => 'NORTH AMERICA',
		),
		'GE' => 
		array (
			'code' => 'GE',
			'lat' => '42.0000',
			'lon' => '43.5000',
			'name' => 'Georgia',
			'region' => 'MIDDLE EAST',
		),
		'GF' => 
		array (
			'code' => 'GF',
			'lat' => '4.0000',
			'lon' => '-53.0000',
			'name' => 'French Guiana',
			'region' => 'SOUTH AMERICA',
		),
		'GH' => 
		array (
			'code' => 'GH',
			'lat' => '8.0000',
			'lon' => '-2.0000',
			'name' => 'Ghana',
			'region' => 'AFRICA',
		),
		'GI' => 
		array (
			'code' => 'GI',
			'lat' => '36.1833',
			'lon' => '-5.3667',
			'name' => 'Gibraltar',
			'region' => 'EUROPE',
		),
		'GL' => 
		array (
			'code' => 'GL',
			'lat' => '72.0000',
			'lon' => '-40.0000',
			'name' => 'Greenland',
			'region' => 'NORTH AMERICA',
		),
		'GM' => 
		array (
			'code' => 'GM',
			'lat' => '13.4667',
			'lon' => '-16.5667',
			'name' => 'Gambia',
			'region' => 'AFRICA',
		),
		'GN' => 
		array (
			'code' => 'GN',
			'lat' => '11.0000',
			'lon' => '-10.0000',
			'name' => 'Guinea',
			'region' => 'AFRICA',
		),
		'GP' => 
		array (
			'code' => 'GP',
			'lat' => '16.2500',
			'lon' => '-61.5833',
			'name' => 'Guadeloupe',
			'region' => 'NORTH AMERICA',
		),
		'GQ' => 
		array (
			'code' => 'GQ',
			'lat' => '2.0000',
			'lon' => '10.0000',
			'name' => 'Equatorial Guinea',
			'region' => 'AFRICA',
		),
		'GR' => 
		array (
			'code' => 'GR',
			'lat' => '39.0000',
			'lon' => '22.0000',
			'name' => 'Greece',
			'region' => 'EUROPE',
		),
		'GT' => 
		array (
			'code' => 'GT',
			'lat' => '15.5000',
			'lon' => '-90.2500',
			'name' => 'Guatemala',
			'region' => 'NORTH AMERICA',
		),
		'GU' => 
		array (
			'code' => 'GU',
			'lat' => '13.4667',
			'lon' => '144.7833',
			'name' => 'Guam',
			'region' => 'OCEANIA',
		),
		'GW' => 
		array (
			'code' => 'GW',
			'lat' => '12.0000',
			'lon' => '-15.0000',
			'name' => 'Guinea-Bissau',
			'region' => 'AFRICA',
		),
		'GY' => 
		array (
			'code' => 'GY',
			'lat' => '5.0000',
			'lon' => '-59.0000',
			'name' => 'Guyana',
			'region' => 'SOUTH AMERICA',
		),
		'HK' => 
		array (
			'code' => 'HK',
			'lat' => '22.2500',
			'lon' => '114.1667',
			'name' => 'Hong Kong',
			'region' => 'ASIA',
		),
		'HN' => 
		array (
			'code' => 'HN',
			'lat' => '15.0000',
			'lon' => '-86.5000',
			'name' => 'Honduras',
			'region' => 'NORTH AMERICA',
		),
		'HR' => 
		array (
			'code' => 'HR',
			'lat' => '45.1667',
			'lon' => '15.5000',
			'name' => 'Croatia',
			'region' => 'EUROPE',
		),
		'HT' => 
		array (
			'code' => 'HT',
			'lat' => '19.0000',
			'lon' => '-72.4167',
			'name' => 'Haiti',
			'region' => 'NORTH AMERICA',
		),
		'HU' => 
		array (
			'code' => 'HU',
			'lat' => '47.0000',
			'lon' => '20.0000',
			'name' => 'Hungary',
			'region' => 'EUROPE',
		),
		'ID' => 
		array (
			'code' => 'ID',
			'lat' => '-5.0000',
			'lon' => '120.0000',
			'name' => 'Indonesia',
			'region' => 'ASIA',
		),
		'IE' => 
		array (
			'code' => 'IE',
			'lat' => '53.0000',
			'lon' => '-8.0000',
			'name' => 'Ireland',
			'region' => 'EUROPE',
		),
		'IL' => 
		array (
			'code' => 'IL',
			'lat' => '31.5000',
			'lon' => '34.7500',
			'name' => 'Israel',
			'region' => 'MIDDLE EAST',
		),
		'IN' => 
		array (
			'code' => 'IN',
			'lat' => '20.0000',
			'lon' => '77.0000',
			'name' => 'India',
			'region' => 'ASIA',
		),
		'IO' => 
		array (
			'code' => 'IO',
			'lat' => '-6.0000',
			'lon' => '71.5000',
			'name' => 'British Indian Ocean Territory',
			'region' => 'ASIA',
		),
		'IQ' => 
		array (
			'code' => 'IQ',
			'lat' => '33.0000',
			'lon' => '44.0000',
			'name' => 'Iraq',
			'region' => 'MIDDLE EAST',
		),
		'IR' => 
		array (
			'code' => 'IR',
			'lat' => '32.0000',
			'lon' => '53.0000',
			'name' => 'Iran',
			'region' => 'ASIA',
		),
		'IS' => 
		array (
			'code' => 'IS',
			'lat' => '65.0000',
			'lon' => '-18.0000',
			'name' => 'Iceland',
			'region' => 'EUROPE',
		),
		'IT' => 
		array (
			'code' => 'IT',
			'lat' => '42.8333',
			'lon' => '12.8333',
			'name' => 'Italy',
			'region' => 'EUROPE',
		),
		'JM' => 
		array (
			'code' => 'JM',
			'lat' => '18.2500',
			'lon' => '-77.5000',
			'name' => 'Jamaica',
			'region' => 'NORTH AMERICA',
		),
		'JO' => 
		array (
			'code' => 'JO',
			'lat' => '31.0000',
			'lon' => '36.0000',
			'name' => 'Jordan',
			'region' => 'MIDDLE EAST',
		),
		'JP' => 
		array (
			'code' => 'JP',
			'lat' => '36.0000',
			'lon' => '138.0000',
			'name' => 'Japan',
			'region' => 'ASIA',
		),
		'KE' => 
		array (
			'code' => 'KE',
			'lat' => '1.0000',
			'lon' => '38.0000',
			'name' => 'Kenya',
			'region' => 'AFRICA',
		),
		'KG' => 
		array (
			'code' => 'KG',
			'lat' => '41.0000',
			'lon' => '75.0000',
			'name' => 'Kyrgyzstan',
			'region' => 'ASIA',
		),
		'KH' => 
		array (
			'code' => 'KH',
			'lat' => '13.0000',
			'lon' => '105.0000',
			'name' => 'Cambodia',
			'region' => 'ASIA',
		),
		'KI' => 
		array (
			'code' => 'KI',
			'lat' => '1.4167',
			'lon' => '173.0000',
			'name' => 'Kiribati',
			'region' => 'OCEANIA',
		),
		'KM' => 
		array (
			'code' => 'KM',
			'lat' => '-12.1667',
			'lon' => '44.2500',
			'name' => 'Comoros',
			'region' => 'AFRICA',
		),
		'KN' => 
		array (
			'code' => 'KN',
			'lat' => '17.3333',
			'lon' => '-62.7500',
			'name' => 'Saint Kitts and Nevis',
			'region' => 'NORTH AMERICA',
		),
		'KP' => 
		array (
			'code' => 'KP',
			'lat' => '40.0000',
			'lon' => '127.0000',
			'name' => 'North Korea',
			'region' => 'ASIA',
		),
		'KR' => 
		array (
			'code' => 'KR',
			'lat' => '37.0000',
			'lon' => '127.5000',
			'name' => 'South Korea',
			'region' => 'ASIA',
		),
		'KW' => 
		array (
			'code' => 'KW',
			'lat' => '29.3375',
			'lon' => '47.6581',
			'name' => 'Kuwait',
			'region' => 'MIDDLE EAST',
		),
		'KY' => 
		array (
			'code' => 'KY',
			'lat' => '19.5000',
			'lon' => '-80.5000',
			'name' => 'Cayman Islands',
			'region' => 'NORTH AMERICA',
		),
		'KZ' => 
		array (
			'code' => 'KZ',
			'lat' => '48.0000',
			'lon' => '68.0000',
			'name' => 'Kazakhstan',
			'region' => 'ASIA',
		),
		'LA' => 
		array (
			'code' => 'LA',
			'lat' => '18.0000',
			'lon' => '105.0000',
			'name' => 'Laos',
			'region' => 'ASIA',
		),
		'LB' => 
		array (
			'code' => 'LB',
			'lat' => '33.8333',
			'lon' => '35.8333',
			'name' => 'Lebanon',
			'region' => 'MIDDLE EAST',
		),
		'LC' => 
		array (
			'code' => 'LC',
			'lat' => '13.8833',
			'lon' => '-61.1333',
			'name' => 'Saint Lucia',
			'region' => 'NORTH AMERICA',
		),
		'LI' => 
		array (
			'code' => 'LI',
			'lat' => '47.1667',
			'lon' => '9.5333',
			'name' => 'Liechtenstein',
			'region' => 'EUROPE',
		),
		'LK' => 
		array (
			'code' => 'LK',
			'lat' => '7.0000',
			'lon' => '81.0000',
			'name' => 'Sri Lanka',
			'region' => 'ASIA',
		),
		'LR' => 
		array (
			'code' => 'LR',
			'lat' => '6.5000',
			'lon' => '-9.5000',
			'name' => 'Liberia',
			'region' => 'AFRICA',
		),
		'LS' => 
		array (
			'code' => 'LS',
			'lat' => '-29.5000',
			'lon' => '28.5000',
			'name' => 'Lesotho',
			'region' => 'AFRICA',
		),
		'LT' => 
		array (
			'code' => 'LT',
			'lat' => '56.0000',
			'lon' => '24.0000',
			'name' => 'Lithuania',
			'region' => 'EUROPE',
		),
		'LU' => 
		array (
			'code' => 'LU',
			'lat' => '49.7500',
			'lon' => '6.1667',
			'name' => 'Luxembourg',
			'region' => 'EUROPE',
		),
		'LV' => 
		array (
			'code' => 'LV',
			'lat' => '57.0000',
			'lon' => '25.0000',
			'name' => 'Latvia',
			'region' => 'EUROPE',
		),
		'LY' => 
		array (
			'code' => 'LY',
			'lat' => '25.0000',
			'lon' => '17.0000',
			'name' => 'Libya',
			'region' => 'AFRICA',
		),
		'MA' => 
		array (
			'code' => 'MA',
			'lat' => '32.0000',
			'lon' => '-5.0000',
			'name' => 'Morocco',
			'region' => 'AFRICA',
		),
		'MC' => 
		array (
			'code' => 'MC',
			'lat' => '43.7333',
			'lon' => '7.4000',
			'name' => 'Monaco',
			'region' => 'EUROPE',
		),
		'MD' => 
		array (
			'code' => 'MD',
			'lat' => '47.0000',
			'lon' => '29.0000',
			'name' => 'Moldova',
			'region' => 'EUROPE',
		),
		'ME' => 
		array (
			'code' => 'ME',
			'lat' => '42.0000',
			'lon' => '19.0000',
			'name' => 'Montenegro',
			'region' => 'EUROPE',
		),
		'MG' => 
		array (
			'code' => 'MG',
			'lat' => '-20.0000',
			'lon' => '47.0000',
			'name' => 'Madagascar',
			'region' => 'AFRICA',
		),
		'MH' => 
		array (
			'code' => 'MH',
			'lat' => '9.0000',
			'lon' => '168.0000',
			'name' => 'Marshall Islands',
			'region' => 'OCEANIA',
		),
		'MK' => 
		array (
			'code' => 'MK',
			'lat' => '41.8333',
			'lon' => '22.0000',
			'name' => 'Macedonia',
			'region' => 'EUROPE',
		),
		'ML' => 
		array (
			'code' => 'ML',
			'lat' => '17.0000',
			'lon' => '-4.0000',
			'name' => 'Mali',
			'region' => 'AFRICA',
		),
		'MM' => 
		array (
			'code' => 'MM',
			'lat' => '22.0000',
			'lon' => '98.0000',
			'name' => 'Burma',
			'region' => 'ASIA',
		),
		'MN' => 
		array (
			'code' => 'MN',
			'lat' => '46.0000',
			'lon' => '105.0000',
			'name' => 'Mongolia',
			'region' => 'ASIA',
		),
		'MO' => 
		array (
			'code' => 'MO',
			'lat' => '22.1667',
			'lon' => '113.5500',
			'name' => 'Macau',
			'region' => 'ASIA',
		),
		'MP' => 
		array (
			'code' => 'MP',
			'lat' => '15.2000',
			'lon' => '145.7500',
			'name' => 'Northern Mariana Islands',
			'region' => 'OCEANIA',
		),
		'MQ' => 
		array (
			'code' => 'MQ',
			'lat' => '14.6667',
			'lon' => '-61.0000',
			'name' => 'Martinique',
			'region' => 'NORTH AMERICA',
		),
		'MR' => 
		array (
			'code' => 'MR',
			'lat' => '20.0000',
			'lon' => '-12.0000',
			'name' => 'Mauritania',
			'region' => 'AFRICA',
		),
		'MS' => 
		array (
			'code' => 'MS',
			'lat' => '16.7500',
			'lon' => '-62.2000',
			'name' => 'Montserrat',
			'region' => 'NORTH AMERICA',
		),
		'MT' => 
		array (
			'code' => 'MT',
			'lat' => '35.8333',
			'lon' => '14.5833',
			'name' => 'Malta',
			'region' => 'EUROPE',
		),
		'MU' => 
		array (
			'code' => 'MU',
			'lat' => '-20.2833',
			'lon' => '57.5500',
			'name' => 'Mauritius',
			'region' => 'AFRICA',
		),
		'MV' => 
		array (
			'code' => 'MV',
			'lat' => '3.2500',
			'lon' => '73.0000',
			'name' => 'Maldives',
			'region' => 'ASIA',
		),
		'MW' => 
		array (
			'code' => 'MW',
			'lat' => '-13.5000',
			'lon' => '34.0000',
			'name' => 'Malawi',
			'region' => 'AFRICA',
		),
		'MX' => 
		array (
			'code' => 'MX',
			'lat' => '23.0000',
			'lon' => '-102.0000',
			'name' => 'Mexico',
			'region' => 'NORTH AMERICA',
		),
		'MY' => 
		array (
			'code' => 'MY',
			'lat' => '2.5000',
			'lon' => '112.5000',
			'name' => 'Malaysia',
			'region' => 'ASIA',
		),
		'MZ' => 
		array (
			'code' => 'MZ',
			'lat' => '-18.2500',
			'lon' => '35.0000',
			'name' => 'Mozambique',
			'region' => 'AFRICA',
		),
		'NA' => 
		array (
			'code' => 'NA',
			'lat' => '-22.0000',
			'lon' => '17.0000',
			'name' => 'Namibia',
			'region' => 'AFRICA',
		),
		'NC' => 
		array (
			'code' => 'NC',
			'lat' => '-21.5000',
			'lon' => '165.5000',
			'name' => 'New Caledonia',
			'region' => 'OCEANIA',
		),
		'NE' => 
		array (
			'code' => 'NE',
			'lat' => '16.0000',
			'lon' => '8.0000',
			'name' => 'Niger',
			'region' => 'AFRICA',
		),
		'NF' => 
		array (
			'code' => 'NF',
			'lat' => '-29.0333',
			'lon' => '167.9500',
			'name' => 'Norfolk Island',
			'region' => 'OCEANIA',
		),
		'NG' => 
		array (
			'code' => 'NG',
			'lat' => '10.0000',
			'lon' => '8.0000',
			'name' => 'Nigeria',
			'region' => 'AFRICA',
		),
		'NI' => 
		array (
			'code' => 'NI',
			'lat' => '13.0000',
			'lon' => '-85.0000',
			'name' => 'Nicaragua',
			'region' => 'NORTH AMERICA',
		),
		'NL' => 
		array (
			'code' => 'NL',
			'lat' => '52.5000',
			'lon' => '5.7500',
			'name' => 'Netherlands',
			'region' => 'EUROPE',
		),
		'NO' => 
		array (
			'code' => 'NO',
			'lat' => '62.0000',
			'lon' => '10.0000',
			'name' => 'Norway',
			'region' => 'EUROPE',
		),
		'NP' => 
		array (
			'code' => 'NP',
			'lat' => '28.0000',
			'lon' => '84.0000',
			'name' => 'Nepal',
			'region' => 'ASIA',
		),
		'NR' => 
		array (
			'code' => 'NR',
			'lat' => '-0.5333',
			'lon' => '166.9167',
			'name' => 'Nauru',
			'region' => 'OCEANIA',
		),
		'NU' => 
		array (
			'code' => 'NU',
			'lat' => '-19.0333',
			'lon' => '-169.8667',
			'name' => 'Niue',
			'region' => 'OCEANIA',
		),
		'NZ' => 
		array (
			'code' => 'NZ',
			'lat' => '-41.0000',
			'lon' => '174.0000',
			'name' => 'New Zealand',
			'region' => 'OCEANIA',
		),
		'OM' => 
		array (
			'code' => 'OM',
			'lat' => '21.0000',
			'lon' => '57.0000',
			'name' => 'Oman',
			'region' => 'MIDDLE EAST',
		),
		'PA' => 
		array (
			'code' => 'PA',
			'lat' => '9.0000',
			'lon' => '-80.0000',
			'name' => 'Panama',
			'region' => 'NORTH AMERICA',
		),
		'PE' => 
		array (
			'code' => 'PE',
			'lat' => '-10.0000',
			'lon' => '-76.0000',
			'name' => 'Perú',
			'region' => 'SOUTH AMERICA',
		),
		'PF' => 
		array (
			'code' => 'PF',
			'lat' => '-15.0000',
			'lon' => '-140.0000',
			'name' => 'French Polynesia',
			'region' => 'OCEANIA',
		),
		'PG' => 
		array (
			'code' => 'PG',
			'lat' => '-6.0000',
			'lon' => '147.0000',
			'name' => 'Papua New Guinea',
			'region' => 'OCEANIA',
		),
		'PH' => 
		array (
			'code' => 'PH',
			'lat' => '13.0000',
			'lon' => '122.0000',
			'name' => 'Philippines',
			'region' => 'ASIA',
		),
		'PK' => 
		array (
			'code' => 'PK',
			'lat' => '30.0000',
			'lon' => '70.0000',
			'name' => 'Pakistan',
			'region' => 'ASIA',
		),
		'PL' => 
		array (
			'code' => 'PL',
			'lat' => '52.0000',
			'lon' => '20.0000',
			'name' => 'Poland',
			'region' => 'EUROPE',
		),
		'PM' => 
		array (
			'code' => 'PM',
			'lat' => '46.8333',
			'lon' => '-56.3333',
			'name' => 'Saint Pierre and Miquelon',
			'region' => 'NORTH AMERICA',
		),
		'PR' => 
		array (
			'code' => 'PR',
			'lat' => '18.2500',
			'lon' => '-66.5000',
			'name' => 'Puerto Rico',
			'region' => 'NORTH AMERICA',
		),
		'PS' => 
		array (
			'code' => 'PS',
			'lat' => '32.0000',
			'lon' => '35.2500',
			'name' => 'Palestinian Territory',
			'region' => 'MIDDLE EAST',
		),
		'PT' => 
		array (
			'code' => 'PT',
			'lat' => '39.5000',
			'lon' => '-8.0000',
			'name' => 'Portugal',
			'region' => 'EUROPE',
		),
		'PW' => 
		array (
			'code' => 'PW',
			'lat' => '7.5000',
			'lon' => '134.5000',
			'name' => 'Palau',
			'region' => 'OCEANIA',
		),
		'PY' => 
		array (
			'code' => 'PY',
			'lat' => '-23.0000',
			'lon' => '-58.0000',
			'name' => 'Paraguay',
			'region' => 'SOUTH AMERICA',
		),
		'QA' => 
		array (
			'code' => 'QA',
			'lat' => '25.5000',
			'lon' => '51.2500',
			'name' => 'Qatar',
			'region' => 'MIDDLE EAST',
		),
		'RE' => 
		array (
			'code' => 'RE',
			'lat' => '-21.1000',
			'lon' => '55.6000',
			'name' => 'Réunion',
			'region' => 'AFRICA',
		),
		'RO' => 
		array (
			'code' => 'RO',
			'lat' => '46.0000',
			'lon' => '25.0000',
			'name' => 'Romania',
			'region' => 'EUROPE',
		),
		'RS' => 
		array (
			'code' => 'RS',
			'lat' => '44.0000',
			'lon' => '21.0000',
			'name' => 'Serbia',
			'region' => 'EUROPE',
		),
		'RU' => 
		array (
			'code' => 'RU',
			'lat' => '60.0000',
			'lon' => '100.0000',
			'name' => 'Russia',
			'region' => 'EUROPE',
		),
		'RW' => 
		array (
			'code' => 'RW',
			'lat' => '-2.0000',
			'lon' => '30.0000',
			'name' => 'Rwanda',
			'region' => 'AFRICA',
		),
		'SA' => 
		array (
			'code' => 'SA',
			'lat' => '25.0000',
			'lon' => '45.0000',
			'name' => 'Saudi Arabia',
			'region' => 'MIDDLE EAST',
		),
		'SB' => 
		array (
			'code' => 'SB',
			'lat' => '-8.0000',
			'lon' => '159.0000',
			'name' => 'Solomon Islands',
			'region' => 'OCEANIA',
		),
		'SC' => 
		array (
			'code' => 'SC',
			'lat' => '-4.5833',
			'lon' => '55.6667',
			'name' => 'Seychelles',
			'region' => 'AFRICA',
		),
		'SD' => 
		array (
			'code' => 'SD',
			'lat' => '15.0000',
			'lon' => '30.0000',
			'name' => 'Sudan',
			'region' => 'AFRICA',
		),
		'SE' => 
		array (
			'code' => 'SE',
			'lat' => '62.0000',
			'lon' => '15.0000',
			'name' => 'Sweden',
			'region' => 'EUROPE',
		),
		'SG' => 
		array (
			'code' => 'SG',
			'lat' => '1.3667',
			'lon' => '103.8000',
			'name' => 'Singapore',
			'region' => 'ASIA',
		),
		'SH' => 
		array (
			'code' => 'SH',
			'lat' => '-15.9333',
			'lon' => '-5.7000',
			'name' => 'Saint Helena',
			'region' => 'AFRICA',
		),
		'SI' => 
		array (
			'code' => 'SI',
			'lat' => '46.0000',
			'lon' => '15.0000',
			'name' => 'Slovenia',
			'region' => 'EUROPE',
		),
		'SK' => 
		array (
			'code' => 'SK',
			'lat' => '48.6667',
			'lon' => '19.5000',
			'name' => 'Slovakia',
			'region' => 'EUROPE',
		),
		'SL' => 
		array (
			'code' => 'SL',
			'lat' => '8.5000',
			'lon' => '-11.5000',
			'name' => 'Sierra Leone',
			'region' => 'AFRICA',
		),
		'SM' => 
		array (
			'code' => 'SM',
			'lat' => '43.7667',
			'lon' => '12.4167',
			'name' => 'San Marino',
			'region' => 'EUROPE',
		),
		'SN' => 
		array (
			'code' => 'SN',
			'lat' => '14.0000',
			'lon' => '-14.0000',
			'name' => 'Senegal',
			'region' => 'AFRICA',
		),
		'SO' => 
		array (
			'code' => 'SO',
			'lat' => '10.0000',
			'lon' => '49.0000',
			'name' => 'Somalia',
			'region' => 'AFRICA',
		),
		'SR' => 
		array (
			'code' => 'SR',
			'lat' => '4.0000',
			'lon' => '-56.0000',
			'name' => 'Suriname',
			'region' => 'SOUTH AMERICA',
		),
		'ST' => 
		array (
			'code' => 'ST',
			'lat' => '1.0000',
			'lon' => '7.0000',
			'name' => 'São Tomé and Principe',
			'region' => 'AFRICA',
		),
		'SV' => 
		array (
			'code' => 'SV',
			'lat' => '13.8333',
			'lon' => '-88.9167',
			'name' => 'El Salvador',
			'region' => 'NORTH AMERICA',
		),
		'SY' => 
		array (
			'code' => 'SY',
			'lat' => '35.0000',
			'lon' => '38.0000',
			'name' => 'Syria',
			'region' => 'MIDDLE EAST',
		),
		'SZ' => 
		array (
			'code' => 'SZ',
			'lat' => '-26.5000',
			'lon' => '31.5000',
			'name' => 'Swaziland',
			'region' => 'AFRICA',
		),
		'TC' => 
		array (
			'code' => 'TC',
			'lat' => '21.7500',
			'lon' => '-71.5833',
			'name' => 'Turks and Caicos Islands',
			'region' => 'NORTH AMERICA',
		),
		'TD' => 
		array (
			'code' => 'TD',
			'lat' => '15.0000',
			'lon' => '19.0000',
			'name' => 'Chad',
			'region' => 'AFRICA',
		),
		'TG' => 
		array (
			'code' => 'TG',
			'lat' => '8.0000',
			'lon' => '1.1667',
			'name' => 'Togo',
			'region' => 'AFRICA',
		),
		'TH' => 
		array (
			'code' => 'TH',
			'lat' => '15.0000',
			'lon' => '100.0000',
			'name' => 'Thailand',
			'region' => 'ASIA',
		),
		'TJ' => 
		array (
			'code' => 'TJ',
			'lat' => '39.0000',
			'lon' => '71.0000',
			'name' => 'Tajikistan',
			'region' => 'ASIA',
		),
		'TK' => 
		array (
			'code' => 'TK',
			'lat' => '-9.0000',
			'lon' => '-172.0000',
			'name' => 'Tokelau',
			'region' => 'OCEANIA',
		),
		'TM' => 
		array (
			'code' => 'TM',
			'lat' => '40.0000',
			'lon' => '60.0000',
			'name' => 'Turkmenistan',
			'region' => 'ASIA',
		),
		'TN' => 
		array (
			'code' => 'TN',
			'lat' => '34.0000',
			'lon' => '9.0000',
			'name' => 'Tunisia',
			'region' => 'AFRICA',
		),
		'TO' => 
		array (
			'code' => 'TO',
			'lat' => '-20.0000',
			'lon' => '-175.0000',
			'name' => 'Tonga',
			'region' => 'OCEANIA',
		),
		'TR' => 
		array (
			'code' => 'TR',
			'lat' => '39.0000',
			'lon' => '35.0000',
			'name' => 'Turkey',
			'region' => 'MIDDLE EAST',
		),
		'TT' => 
		array (
			'code' => 'TT',
			'lat' => '11.0000',
			'lon' => '-61.0000',
			'name' => 'Trinidad and Tobago',
			'region' => 'NORTH AMERICA',
		),
		'TV' => 
		array (
			'code' => 'TV',
			'lat' => '-8.0000',
			'lon' => '178.0000',
			'name' => 'Tuvalu',
			'region' => 'OCEANIA',
		),
		'TW' => 
		array (
			'code' => 'TW',
			'lat' => '23.5000',
			'lon' => '121.0000',
			'name' => 'Taiwan',
			'region' => 'ASIA',
		),
		'TZ' => 
		array (
			'code' => 'TZ',
			'lat' => '-6.0000',
			'lon' => '35.0000',
			'name' => 'Tanzania',
			'region' => 'AFRICA',
		),
		'UA' => 
		array (
			'code' => 'UA',
			'lat' => '49.0000',
			'lon' => '32.0000',
			'name' => 'Ukraine',
			'region' => 'EUROPE',
		),
		'UG' => 
		array (
			'code' => 'UG',
			'lat' => '1.0000',
			'lon' => '32.0000',
			'name' => 'Uganda',
			'region' => 'AFRICA',
		),
		'UM' => 
		array (
			'code' => 'UM',
			'lat' => '19.2833',
			'lon' => '166.6000',
			'name' => 'United States Minor Outlying Islands',
			'region' => 'OCEANIA',
		),
		'US' => 
		array (
			'code' => 'US',
			'lat' => '38.0000',
			'lon' => '-97.0000',
			'name' => 'United States',
			'region' => 'NORTH AMERICA',
		),
		'UY' => 
		array (
			'code' => 'UY',
			'lat' => '-33.0000',
			'lon' => '-56.0000',
			'name' => 'Uruguay',
			'region' => 'SOUTH AMERICA',
		),
		'UZ' => 
		array (
			'code' => 'UZ',
			'lat' => '41.0000',
			'lon' => '64.0000',
			'name' => 'Uzbekistan',
			'region' => 'ASIA',
		),
		'VA' => 
		array (
			'code' => 'VA',
			'lat' => '41.9000',
			'lon' => '12.4500',
			'name' => 'Vatican City',
			'region' => 'EUROPE',
		),
		'VC' => 
		array (
			'code' => 'VC',
			'lat' => '13.2500',
			'lon' => '-61.2000',
			'name' => 'Saint Vincent and the Grenadines',
			'region' => 'NORTH AMERICA',
		),
		'VE' => 
		array (
			'code' => 'VE',
			'lat' => '8.0000',
			'lon' => '-66.0000',
			'name' => 'Venezuela',
			'region' => 'SOUTH AMERICA',
		),
		'VG' => 
		array (
			'code' => 'VG',
			'lat' => '18.5000',
			'lon' => '-64.5000',
			'name' => 'British Virgin Islands',
			'region' => 'NORTH AMERICA',
		),
		'VI' => 
		array (
			'code' => 'VI',
			'lat' => '18.3333',
			'lon' => '-64.8333',
			'name' => 'U.S. Virgin Islands',
			'region' => 'NORTH AMERICA',
		),
		'VN' => 
		array (
			'code' => 'VN',
			'lat' => '16.0000',
			'lon' => '106.0000',
			'name' => 'Vietnam',
			'region' => 'ASIA',
		),
		'VU' => 
		array (
			'code' => 'VU',
			'lat' => '-16.0000',
			'lon' => '167.0000',
			'name' => 'Vanuatu',
			'region' => 'OCEANIA',
		),
		'WF' => 
		array (
			'code' => 'WF',
			'lat' => '-13.3000',
			'lon' => '-176.2000',
			'name' => 'Wallis and Futuna',
			'region' => 'OCEANIA',
		),
		'WS' => 
		array (
			'code' => 'WS',
			'lat' => '-13.5833',
			'lon' => '-172.3333',
			'name' => 'Samoa',
			'region' => 'OCEANIA',
		),
		'YE' => 
		array (
			'code' => 'YE',
			'lat' => '15.0000',
			'lon' => '48.0000',
			'name' => 'Yemen',
			'region' => 'MIDDLE EAST',
		),
		'YT' => 
		array (
			'code' => 'YT',
			'lat' => '-12.8333',
			'lon' => '45.1667',
			'name' => 'Mayotte',
			'region' => 'AFRICA',
		),
		'ZA' => 
		array (
			'code' => 'ZA',
			'lat' => '-29.0000',
			'lon' => '24.0000',
			'name' => 'South Africa',
			'region' => 'AFRICA',
		),
		'ZM' => 
		array (
			'code' => 'ZM',
			'lat' => '-15.0000',
			'lon' => '30.0000',
			'name' => 'Zambia',
			'region' => 'AFRICA',
		),
		'ZW' => 
		array (
			'code' => 'ZW',
			'lat' => '-20.0000',
			'lon' => '30.0000',
			'name' => 'Zimbabwe',
			'region' => 'AFRICA',
		),
		'GG' => 
		array (
			'code' => 'GG',
			'lat' => '49.45',
			'lon' => '-2.58',
			'name' => 'Guernsey',
			'region' => 'EUROPE',
		),
		'CW' => 
		array (
			'code' => 'CW',
			'name' => 'Curaçao',
			'lat' => '12.1166666667',
			'lon' => '-68.9333333333',
			'region' => 'NORTH AMERICA',
		),
		'BL' => 
		array (
			'code' => 'BL',
			'lat' => '17.9',
			'lon' => '-62.8333',
			'name' => 'Saint Barthélemy',
			'region' => 'NORTH AMERICA',
		),
		'MF' => 
		array (
			'code' => 'MF',
			'lat' => '18.05',
			'lon' => '-63.08',
			'name' => 'Saint Martin',
			'region' => 'NORTH AMERICA',
		),
		'JE' => 
		array (
			'code' => 'JE',
			'lat' => '49.2167',
			'lon' => '-2.1167',
			'name' => 'Jersey',
			'region' => 'EUROPE',
		),
		'SX' => 
		array (
			'code' => 'SX',
			'lat' => '18.08255',
			'lon' => '-63.052251',
			'name' => 'Sint Maarten',
			'region' => 'NORTH AMERICA',
		),
		'TL' => 
		array (
			'code' => 'TL',
			'lat' => '-8.5',
			'lon' => '125.55',
			'name' => 'Timor-Leste',
			'region' => 'ASIA',
		),
		'PN' => 
		array (
			'code' => 'PN',
			'lat' => '-24.36146',
			'lon' => '-128.316376',
			'name' => 'Pitcairn',
			'region' => 'OCEANIA',
		),
		'BQ' => 
		array (
			'code' => 'BQ',
			'lat' => '12.1500000',
			'lon' => '-68.2666700',
			'name' => 'Caribbean Netherlands',
			'region' => 'NORTH AMERICA',
		),
		'IM' => 
		array (
			'code' => 'IM',
			'lat' => '54.23',
			'lon' => '-4.57',
			'name' => 'Isle of Man',
			'region' => 'EUROPE',
		),
		'SS' => 
		array (
			'code' => 'SS',
			'lat' => '4.85',
			'lon' => '31.6',
			'name' => 'South Sudan',
			'region' => 'AFRICA',
		),
		'AQ' => 
		array (
			'name' => 'Antarctica',
			'region' => '(NOT INCLUDED)',
		),
		'TF' => 
		array (
			'name' => 'French Southern Territories',
			'region' => '(NOT INCLUDED)',
		),
		'GS' => 
		array (
			'name' => 'South Georgia and the South Sandwich Islands',
			'region' => '(NOT INCLUDED)',
		),
		'ZZ' => 
		array (
			'name' => 'Unknown or unassigned country',
			'region' => '(NOT INCLUDED)',
		),
		'KS' => 
		array (
			'name' => 'Kosovo',
			'region' => 'EUROPE',
		),
	);

	if($return_array) {
		uasort($array, create_function('$a, $b', 'return strcmp($a["name"], $b["name"]);'));
		return $array;
	}

	if('region' == $info && !isset($array[$code][$info]))
		return 'ORPHANED';

	return isset($array[$code][$info]) ? $array[$code][$info] : '';
}
