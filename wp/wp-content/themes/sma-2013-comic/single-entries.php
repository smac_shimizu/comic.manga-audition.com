<?php
/**
 * Template Name: Entries Single page Template
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

function sma2013comic_enqueue_single_entries_scripts() {
    wp_enqueue_script( 'jquery-swiper', 'http://www.manga-audition.com/viewer/js/swiper.js', array('jquery'), null, true );
    wp_enqueue_script( 'viewer-functions', 'http://www.manga-audition.com/viewer/js/function.js', array('jquery', 'jquery-swiper'), null, true );
    wp_enqueue_style( 'swiper-css', 'http://www.manga-audition.com/viewer/css/swiper.css' );
    wp_enqueue_style( 'custum-css', 'http://www.manga-audition.com/viewer/css/custum.css' );
}
add_action( 'wp_enqueue_scripts', 'sma2013comic_enqueue_single_entries_scripts' );


header('Access-Control-Allow-Origin: *');

// IPアドレスを取得して変数にセットする
$ipAddress = $_SERVER["REMOTE_ADDR"];
$referer = $_SERVER["HTTP_REFERER"];
$ua = $_SERVER['HTTP_USER_AGENT'];

// IPアドレスを数値として取得する場合
$ipLong = ip2long($ipAddress);

// ファーストページタイプ判定（Sigle Left or Double spread）
$field = get_field_object('the_first_page_is');
$value = get_field('the_first_page_is');
$label = $field['choices'][ $value ];
switch ($label) {
    case 'Please Select':
        $is_first = "nonselect";
        break;
    case 'Single Page (Left page)':
        $is_first = "single";
        break;
    case 'Double Page Spread':
        $is_first = "double";
        break;
}
?>

<?php 
function sma2013comic_get_entry_images_new($post_id = null, $return_urls = false) {
    global $comic, $sma2013comic_cache;

    if(!$post_id){

        return false;       
    }

    if(isset($sma2013comic_cache['entry_image_urls_' . $post_id]) && is_array($sma2013comic_cache['entry_image_urls_' . $post_id])) {

        $urls = $sma2013comic_cache['entry_image_urls_' . $post_id];
    }
    else {
        $entry_category = wp_get_object_terms($post_id, 'entry-category', array('fields' => 'slugs'));
        if(is_array($entry_category) && count($entry_category) === 1)
            $entry_category = $entry_category[0];
        else
            $entry_category = 'unknown';

        if(empty($entry_category))
            $entry_category = 'zma2013';

        if($entry_category == 'zma2013') {
            $entry_id = get_field('entry_id');
            if(empty($entry_id))
                return false;

            $dir = $comic['settings']['entry_image_base_path'] . $entry_category . '/' . substr($entry_id, 3) . '/';
            $dir_url = $comic['settings']['entry_image_base_url'] . $entry_category . '/' . substr($entry_id, 3) . '/';
        }
        else {
            $dir = $comic['settings']['entry_image_base_path'] . $entry_category . '/' . (int) $post_id . '/';
            $dir_url = $comic['settings']['entry_image_base_url'] . $entry_category . '/' . (int) $post_id . '/';
        }

        $urls = array();

        if ( !is_dir($dir) ) {
            return false;
        }
        if ( ! $dh = opendir($dir) ) {
            return false;
        }
        while ( ( $image = readdir( $dh ) ) !== false ) {
            if(false !== strpos($image, '.jpg') || false !== strpos($image, '.png'))
                $urls[] = $dir_url . $image;
        }
        closedir( $dh );
        sort($urls);
        $sma2013comic_cache['entry_image_urls_' . $post_id] = $urls;
    }

    if($return_urls)
        return $urls;

    $output = '';
    $i = 1;

    foreach ($urls as $url) {
        if($i % 2 == 0) {
            $output .= '<div class="swiper-slide even" data-hash="'.$i.'"><img src="' . esc_url($url) . '"></div>';
        } else {
           $output .= '<div class="swiper-slide odd" data-hash="'.$i.'"><img src="' . esc_url($url) . '"></div>';
        }
        $i++;
    }
    return $output;
}


    
function sma2013comic_get_entry_meta_new() {
    sma2013comic_get_penname();

    $output = '<div class="info-meta meta-title">TITLE: ' . sma2013comic_get_title() . '</div>' . "\n";
    $output = '<div class="info-meta meta-author">AUTHOR: ' . sma2013comic_get_penname() . '</div>' . "\n";
    $output = '<div class="info-meta meta-country">COUNTRY: ' . sma2013comic_get_country_code_info(sma2013comic_get_country_code(), 'name') . '</div>' . "\n";
    if($best_part_of_manga = get_field('best_part_of_manga'))
        $output .= '<div class="info-meta meta-text">BEST PART OF MANGA: ' . esc_html($best_part_of_manga) . '</div>' . "\n";
    if($comment = get_field('comment'))
        $output .= '<div class="info-meta meta-text">COMMENT: ' . esc_html($comment) . '</div>' . "\n";
    return $output;
}

function sma2013comic_single_entries_fb_og() {
    global $wp_query;

    $post_id = (int) $wp_query->get_queried_object_id();

    if(!is_singular('entries') || empty($post_id))
        return;

    $title = esc_attr(single_post_title('', false));

    $description = '';
    
    if($best_part_of_manga = get_field('best_part_of_manga'))
        $description .= esc_attr($best_part_of_manga);

    if(empty($description))
        $description .= $title;

    $images = sma2013comic_get_entry_images($post_id, true);
    $image = !empty($images[1]) ? esc_attr($images[1]) : '';

    echo '
<meta property="og:type" content="website" />
<meta property="og:site_name" content="manga-audition.com" />
<meta property="og:description" content="' . $description . '" />
<meta property="og:title" content="'.$title.'" />
<meta property="og:image" content="' . $image . '" />
';
}

?>
<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta name="viewport" content="width = device-width, user-scalable = yes,initial-scale=1.0" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<meta property="fb:admins" content="1850856843" />
<meta property="fb:admins" content="100006060362651" />

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35793729-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<meta name="google-translate-customization" content="ab8431b6f50dd83b-3a07b1dc0bb49e44-gff91becc97226b53-17">

<?php sma2013comic_single_entries_twitter_card(); ?>
<?php sma2013comic_single_entries_fb_og();?>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head(); ?>

</head>
<body onload="dataChk(<?php echo $post->ID;?>,<?php echo $ipLong;?>)">

    <div class="swiper-container" dir="rtl">
        <div class="swiper-wrapper">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php echo sma2013comic_get_entry_images_new($post->ID); ?>
            <?php endwhile; // end of the loop. ?>
        </div><!-- swiper-wrapper -->


        <!-- Add Pagination -->
        <!-- <div class="swiper-pagination"></div> -->
        <!-- Add Navigation -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div> 

    </div><!-- swiper-container -->


<div class="info">
    <a href="#">
        <i><img src="http://www.manga-audition.com/viewer/images/info.png" alt="" /></i>
    </a>
    <div class="info-meta-container">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT, gaTrack: true, gaId: 'UA-35793729-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    <?php while ( have_posts() ) : the_post(); ?>
        <?php echo sma2013comic_get_entry_meta_new(); ?>
    <?php endwhile; // end of the loop. ?>
    </div>
</div>

<div class="end_btn"></div>

<div class="like-container">

    <div class="inner">

        <div>
            <?php if( strstr($_SERVER["REQUEST_URI"],'/ikusa-no-ko') == TRUE) : ?>
            <a class="button_ora" href="http://www.manga-audition.com/comic-zenon/ikusa-no-ko-by-tetsuo-hara/">"Ikusa No Ko" Series Top >></a>
            <?php elseif( strstr($_SERVER["REQUEST_URI"],'/nobo-and-her') == TRUE) : ?>
            <a class="button_ora" href="http://www.manga-audition.com/comic-zenon/nobo-and-her-by-molico-ross/">"Nobo And Her" Series Top >></a>
            <?php elseif( strstr($_SERVER["REQUEST_URI"],'/arte') == TRUE) : ?>
            <a class="button_ora" href="http://www.manga-audition.com/comic-zenon/arte-by-kei-ohkubo/">"Arte" Series Top >></a>
            <?php elseif( strstr($_SERVER["REQUEST_URI"],'/angel-heart') == TRUE) : ?>
            <a class="button_ora" href="http://www.manga-audition.com/comic-zenon/angel-heart-by-tsukasa-hojo/">"ANGEL HEART" Series Top >></a>
            <?php endif; ?>
            <a class="button_blu" href="http://www.manga-audition.com/sma04-2015award/">SMA04 AWARD Winners Top</a>
            <a class="button_blu" href="http://www.manga-audition.com/comic-zenon/">Free MANGA Series Top</a>
            <a class="button_blu" href="<?php echo $_SERVER["HTTP_REFERER"]; ?>">Back to Previous Page</a>
            <a class="button_blu" href="http://www.manga-audition.com/">SMAC!Web Home</a>
        </div>

        <div style="margin-top:20px;">
            <div class="icon" id="like_on">
                <a href="javascript:void(0)" onclick="likeAdd(<?php echo $post->ID;?>,<?php echo $ipLong;?>);">
                    LIKED IT? PUSH THE BUTTON!
                    <i><img src="http://www.manga-audition.com/viewer/images/thumbs_up.png" alt="Like"></i>
                </a>
            </div>
            <div class="icon" id="like_off" style="display:none;">
                <a href="javascript:void(0)" onclick="likeDel(<?php echo $post->ID;?>,<?php echo $ipLong;?>);">
                    THANKS LIKE!!
                    <i class="off"><img src="http://www.manga-audition.com/viewer/images/thumbs_up_off.png" alt="Like"></i>
                </a>
            </div>
            <div class="sns-block">
                <div class="fb-like" data-href="<?php echo the_permalink();?>" data-width="100px" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true" style="top:-4px;"></div>
                <a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-hashtags="<?php echo $title;?>" style="">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            </div>
            <div class="clearfix">
            <form action="">
                <div id="thanks" style="display:none;">Thanks Message!!</div>
                <textarea name="" placeholder="Comment" id="txtarea"></textarea>
                <input type="button" value="SEND COMMENT" onclick="commentAdd(<?php echo $post->ID;?>,<?php echo $ipLong;?>)">
            </form>
            </div>
            <div class="close">×</div>
        </div>            

    </div><!-- inner -->
</div><!-- like-container -->

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.4&appId=686663738013259";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="fb-root"></div>

<script type="text/javascript" src="http://www.manga-audition.com/viewer/js/jquery.min.js"></script>
<?php wp_footer(); ?>

<script>
// Viewer End Function
$(function(){
    swiper.once('ReachEnd', function () {
        // Open End Screen
        setTimeout(function(){
            $('.end_btn').fadeIn();
        },100);

        readcomp(<?php echo $post->ID;?>,<?php echo $ipLong;?>);

    });
});

$(function(){
    $(".end_btn").on("click", function() {
        $(this).fadeOut();  
        $(".like-container").fadeIn();      
    });
});
</script>

</body>
</html>