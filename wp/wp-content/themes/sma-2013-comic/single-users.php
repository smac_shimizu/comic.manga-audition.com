<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

if (!sma2013comic_check_user_can_view_users_home())
	wp_die('You do not have sufficient permissions to access this page.');

if(!is_super_admin() && !current_user_can('extended_editor') && !sma2013comic_check_user_completed_profile_form())
	wp_die('Please complete <a href="' . home_url( 'your-profile/') . '">your profile</a> before getting started.');

$sma2013comic_dashboard_user = get_user_by('id', (int) sma2013comic_get_cerrent_post_slug());

function sma2013comic_enqueue_single_users_scripts() {
	wp_enqueue_style( 'colorbox', get_stylesheet_directory_uri() . '/js/colorbox.css');
	wp_enqueue_script( 'jquery-colorbox', get_stylesheet_directory_uri() . '/js/jquery.colorbox-min.js', array('jquery'), null, true );
	wp_enqueue_script( 'sma2013comic-single-users-custom', get_stylesheet_directory_uri() . '/js/single-users-custom.js', array('jquery-colorbox'), null, true );
}
add_action( 'wp_enqueue_scripts', 'sma2013comic_enqueue_single_users_scripts' );

get_header('users'); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<h1 class="entry-title">Howdy, <?php echo esc_html($sma2013comic_dashboard_user->nickname) . '('; the_title(); echo ')'; ?></h1>
		</header><!-- .entry-header -->
		<div class="entry-content">
			<h2 class="entry-status-title">Entry Status</h2>
				<?php
				$posts = get_posts(array(
					'meta_key' => 'user_login',
					'meta_value' => $sma2013comic_dashboard_user->user_login,
					'post_type' => 'entries',
					'post_status' => array('publish', 'pending', 'draft'),
					'numberposts' => -1,
					));
				if(count($posts)) {
					foreach ($posts as $post) {
						setup_postdata($post);
						$sma2013comic_entry_title_orig = get_field('titile_orig', $post->ID);
						$sma2013comic_entry_category = wp_get_object_terms($post->ID, 'entry-category', array('fields' => 'names'));
						if(is_array($sma2013comic_entry_category) && count($sma2013comic_entry_category) === 1)
							$sma2013comic_entry_category = $sma2013comic_entry_category[0];
						else
							$sma2013comic_entry_category = 'unknown';

						$sma2013comic_entry_status = wp_get_object_terms($post->ID, 'status', array('fields' => 'names'));
						if(is_array($sma2013comic_entry_status))
							$sma2013comic_entry_status = implode(', ', $sma2013comic_entry_status);
				?>
						<ul class="entry-status-box">
							<li>TITLE: <?php echo !empty($sma2013comic_entry_title_orig) ? esc_html($sma2013comic_entry_title_orig) : 'n/a'; ?></li>
							<li>ENTRY CATEGORY: <?php echo !empty($sma2013comic_entry_category) ? esc_html($sma2013comic_entry_category) : 'n/a'; ?></li>
							<li>ENTRY DATE: <?php echo esc_html($post->post_date); ?></li>
							<li>ENTRY ID: <?php echo esc_html($post->ID); ?></li>
							<li>STATUS: <?php echo !empty($sma2013comic_entry_status) ? esc_html($sma2013comic_entry_status) : 'n/a'; ?></li>
							<li>FILES: <?php echo sma2013comic_get_entry_image_links($post->ID); ?></li>
						</ul>
				<?php
					}
				}
				else
					echo '<p>(No entries yet)</p>';

				wp_reset_postdata();
				?>

		</div><!-- .entry-content -->
	</article><!-- #post -->

				<?php comments_template( '/comments-users.php', true ); ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar('users'); ?>
<?php get_footer(); ?>