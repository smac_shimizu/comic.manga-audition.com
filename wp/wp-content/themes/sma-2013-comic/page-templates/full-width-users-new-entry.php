<?php
/**
 * Template Name:  Full-width users New entry
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

if(!is_user_logged_in())
	wp_die('You do not have sufficient permissions to access this page.');

if(!sma2013comic_check_user_completed_profile_form())
	wp_die('Please complete <a href="' . home_url( 'your-profile/') . '">your profile</a> before getting started.');

if ( isset($_POST['fuid']) && isset($_POST['sma2013comic-nonce']) ) {
	if(!wp_verify_nonce($_POST['sma2013comic-nonce'],'send-new-entry'))
		wp_die('Invalid form submission.');
	else
		$sma2013comic_make_new_entry_result = sma2013comic_make_new_entry();

	if(false === $sma2013comic_make_new_entry_result) {
		wp_redirect(home_url( 'make-new-entry/?thankyou=0' ));
		exit;
	}
	else {
		wp_redirect(home_url( 'make-new-entry/?thankyou=1' ));
		exit;
	}
}

function sma2013comic_enqueue_new_entry_scripts() {
	wp_enqueue_style( 'theme-my-login', get_stylesheet_directory_uri() . '/theme-my-login.css');
	wp_enqueue_style( 'plupload-queue', get_stylesheet_directory_uri() . '/js/jquery.plupload.queue/css/jquery.plupload.queue.css');
	wp_enqueue_script( 'plupload-full', get_stylesheet_directory_uri() . '/js/plupload.full.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'plupload-queue', get_stylesheet_directory_uri() . '/js/jquery.plupload.queue/jquery.plupload.queue.min.js', array('plupload-full'), null, true );
	wp_enqueue_script( 'uploader-custom', get_stylesheet_directory_uri() . '/js/uploader-custom.js', array('plupload-queue'), '20150313', true );
}
add_action('wp_enqueue_scripts', 'sma2013comic_enqueue_new_entry_scripts');

get_header('users'); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

		<header class="entry-header">
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</header>

		<?php if (isset($_GET['thankyou'])) : ?>
			<div class="entry-content">
				<?php
					if(empty($_GET['thankyou']))
						echo '<p>An unexpected error occurred.</p>';
					else
						echo '<p>Thank you!</p>';
				?>
			</div>
		<?php else : ?>

		<?php //DEBUG
		echo $comic['settings']['entry_image_base_path']."<br>";
		echo $comic['settings']['entry_image_base_url']."<br>";
		echo $comic['settings']['entry_image_uploads_tmp']."<br>";
		echo $comic['settings']['imgresize_dir']."<br>";
		?>

		<!-- テスト環境のため変更（本環境ではaction="/make-new-entry/"を有効にする） -->
		<!-- <form id="entry-form" method="post" action="/make-new-entry/"> -->
		<form id="entry-form" method="post" action="/wp/make-new-entry/">
		<div class="entry-content" id="entry-class-block">
			<?php echo sma2013comic_entry_class_select_options(); ?>
		</div>

		<div id="uploader-block">
			<div class="entry-content">
			<h2>Uploading Files</h2>
				<ol>
				<li>Name image files, ending with 2 digits zero-padded numbers + a file extension in a sequential manner (ie. MyStory_01.jpg, MyStory_02.jpg ... MyStory_10.jpg). It may display in wrong order if not named correctly.</li>
				<li>Add all files to the upload queue (JPEG or PNG).</li>
				<li>Click the 'Start Upload' button.</li>
				</ol>
			</div>
			<div id="uploader">
				<p>Your browser doesn't have HTML5 support.</p>
			</div>
		</div>

		<div class="entry-content" id="details">
			<h2>Please enter all details in English Readable forms and click the 'Send New Entry' button.</h2>

			<table class="form-table">
			<tr>
				<th><label for="the_first_page_is">The first page is</label></th>
				<td>
					<select name="the_first_page_is" id="the_first_page_is" required>
						<option value="" selected>Please Select</option>
						<option value="1">Single Page (Left page)</option>
						<option value="2">Double Page Spread</option>
					</select>
					<br><span class="description">From 2015, Page order is &quot;RIGHT to LEFT&quot; only.</span>
				</td>
			</tr>
			<tr>
				<th><label for="entry_titile_orig">Title</label></th>
				<td><input type="text" name="entry_titile_orig" id="entry_titile_orig" class="regular-text" required /></td>
			</tr>
			<tr>
				<th><label for="entry_best_part_of_manga">Best Part of Manga</label></th>
				<td><textarea name="entry_best_part_of_manga" id="entry_best_part_of_manga" rows="5" cols="30" required></textarea></td>
			</tr>
			<tr>
				<th><label for="entry_comment">Comment</label></th>
				<td><textarea name="entry_comment" id="entry_comment" rows="5" cols="30"></textarea></td>
			</tr>
			<tr>
				<th><label for="entry_is_this_a_team_effort">Is this a team effort?</label></th>
				<td><input name="entry_is_this_a_team_effort" id="entry_is_this_a_team_effort" value="1" type="checkbox"></td>
			</tr>
			<tr>
				<th><label for="entry_credit">Credit</label></th>
				<td><textarea name="entry_credit" id="entry_credit" rows="5" cols="30"></textarea><br><span class="description">Please describe people you would like to credit with your work ('Story by', 'Inspiration by', etc).</span></td>
			</tr>
			</table>
			<p class="submit">
				<?php wp_nonce_field('send-new-entry','sma2013comic-nonce', false); ?>
				<input id="fuid" type="hidden" value="<?php echo md5(uniqid('', true)); ?>" name="fuid">
				<input type="submit" class="button-primary" value="Send New Entry" />
			</p>
		</div>
		</form>

		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
