<?php
/**
 * Template Name: Full-width users
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

if(current_user_can('guest_reader'))
	wp_die('Logged In as Guests.');

function sma2013comic_enqueue_full_width_users_scripts() {
	wp_enqueue_style('theme-my-login', get_stylesheet_directory_uri() . '/theme-my-login.css');
}
add_action('wp_enqueue_scripts', 'sma2013comic_enqueue_full_width_users_scripts');

get_header('users'); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>