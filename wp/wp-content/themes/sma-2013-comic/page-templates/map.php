<?php
/**
 * Template Name: Map Page Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

wp_redirect('http://www.manga-audition.com/', 301);
exit;

function sma2013comic_enqueue_map_scripts() {
	/*
	<script src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="./src/cmaster.json"></script>
	<script type="text/javascript" src="./src/wp-entries.json"></script>
	<script type="text/javascript" src="./src/markerclusterer_packed.js"></script>
	<script type="text/javascript" src="./src/custom2.js"></script>
	*/
	wp_enqueue_style( 'colorbox', get_stylesheet_directory_uri() . '/js/colorbox.css');
	wp_enqueue_script( 'google-maps', 'http://maps.googleapis.com/maps/api/js?v=3&sensor=false', array(), null, true );
	wp_enqueue_script( 'sma2013comic-map-cmaster', get_stylesheet_directory_uri() . '/js/cmaster.json', array(), null, true );
	wp_enqueue_script( 'sma2013comic-map-wp-entries', get_stylesheet_directory_uri() . '/js/wp-entries.json', array(), null, true );
	wp_enqueue_script( 'markerclusterer', get_stylesheet_directory_uri() . '/js/markerclusterer_packed.js', array('google-maps'), null, true );
	wp_enqueue_script( 'jquery-pagination', get_stylesheet_directory_uri() . '/js/jquery.pagination.js', array('jquery'), null, true );
	wp_enqueue_script( 'sma2013comic-map-custom', get_stylesheet_directory_uri() . '/js/map-custom.js', array('jquery', 'markerclusterer', 'underscore'), null, true );
	wp_enqueue_script( 'jquery-scrollto', get_stylesheet_directory_uri() . '/js/jquery.scrollTo.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'jquery-colorbox', get_stylesheet_directory_uri() . '/js/jquery.colorbox-min.js', array('jquery'), null, true );
	wp_enqueue_script( 'sma2013comic-viewer-custom', get_stylesheet_directory_uri() . '/js/viewer-custom.js', array('jquery', 'jquery-scrollto'), null, true );
}
add_action( 'wp_enqueue_scripts', 'sma2013comic_enqueue_map_scripts' );

get_header(); ?>
<div id='map-message'>
<div id='map-message-left'>Read the entries from around the world!</div>
<div id='map-message-right'><span>Zoom on Map to see more details.</span><br>
<span>Use Area Select to navigate to that area</span></div>
</div>
	<div id="world-map">
  <div id="map-container"><div id="map"></div></div>

</div>

	<div id="primary" class="site-content">
		<div id="content" role="main">

		</div><!-- #content -->
		<div id="Pagination"></div>
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>