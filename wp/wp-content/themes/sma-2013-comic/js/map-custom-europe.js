jQuery(function($) {

// region data
var rc = {
	'AFRICA': { lat: 7.18805555556, lan: 21.0936111111, zoom: 2 },
	'ASIA': { lat: 29.8405555556, lan: 89.2966666667, zoom: 2 },
	'EUROPE': { lat: 48.6908333333, lan: 9.14055555556, zoom: 2 },
	'MIDDLE EAST': { lat: 24.460899, lan: 39.62018999999998, zoom: 2 },
	'NORTH AMERICA': { lat: 46.0730555556, lan: -100.546666667, zoom: 2 },
	'OCEANIA': { lat: -18.3127777778, lan: 138.515555556, zoom: 2 },
	'SOUTH AMERICA': { lat: -14.6047222222, lan: -57.6561111111, zoom: 2 },
	'ORPHANED': { lat: -57.679094, lan: -138.515625, zoom: 2 },
	'WORLD': { lat: 37.4419, lan: -122.1419, zoom: 1 }, // just for lat&lan.
};

// icon data
var icon_style = [{
	url: 'http://comic.manga-audition.com/wp/wp-content/themes/sma-2013-comic/images/c43.png',
	width: 43,
	height: 43,
	textColor: '#ffffff',
	textSize: 14,
	fontWeight: 'normal',
},
{
	url: 'http://comic.manga-audition.com/wp/wp-content/themes/sma-2013-comic/images/c57.png',
	width: 57,
	height: 56,
	textColor: '#ffffff',
	textSize: 16,
	fontWeight: 'normal',
},
{
	url: 'http://comic.manga-audition.com/wp/wp-content/themes/sma-2013-comic/images/c72.png',
	width: 72,
	height: 73,
	textColor: '#ffffff',
	textSize: 18,
	fontWeight: 'normal',
}];

// Marker Groups
var mkgp = {};
mkgp.region = {};
//mkgp.year = {};
//mkgp.cat = {};
mkgp.all = [];
mkgp.region['ASIA'] = [];
mkgp.region['EUROPE'] = [];
mkgp.region['AFRICA'] = [];
mkgp.region['MIDDLE EAST'] = [];
mkgp.region['NORTH AMERICA'] = [];
mkgp.region['OCEANIA'] = [];
mkgp.region['SOUTH AMERICA'] = [];
mkgp.region['ORPHANED'] = [];
//mkgp.year['2013'] = [];
//mkgp.year['2012'] = [];
//mkgp.cat.comedy = [];
//mkgp.cat.action = [];

// functions
var fn = {};
fn.zoom = function(e) {
	e.preventDefault();
	e.stopPropagation();
	var rckey = $(this).data('id'); 
	if (typeof( rc[rckey] ) != 'undefined') {
		if (map.getZoom() !== rc[rckey].zoom) {
			map.setZoom(rc[rckey].zoom);
			map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
		}
		map.setCenter(new google.maps.LatLng(rc[rckey].lat, rc[rckey].lan));
	}
}

fn.fill_marker_groups = function() {
	for (var n in wp_entries) {
		var wp_entry = wp_entries[n];
		if (typeof( cmaster[wp_entry.code] ) != 'undefined') {
			var latLng = new google.maps.LatLng(cmaster[wp_entry.code].lat, cmaster[wp_entry.code].lon);
			var marker = new google.maps.Marker({position: latLng});
			mkgp.all.push(marker);
			//if (typeof( mkgp.year[wp_entry.year] ) != 'undefined')
			//	mkgp.year[wp_entry.year].push(marker);
			//if (typeof( mkgp.cat[wp_entry.cat] ) != 'undefined')
			//	mkgp.cat[wp_entry.cat].push(marker);
			if (typeof( mkgp.region[cmaster[wp_entry.code].region] ) != 'undefined')
				mkgp.region[cmaster[wp_entry.code].region].push(marker);
		}
	}
}

fn.marker_control = function() {
	markerCluster.clearMarkers();
	markerCluster.addMarkers(mkgp.all);

	$('#marker-filter .marker-group').each(function() {
		if (!$(this).prop('checked'))
			markerCluster.removeMarkers(mkgp[$(this).data('mkgp').type][$(this).data('mkgp').id]);
	});
}

// init
var map = new google.maps.Map(document.getElementById('map'), {
	zoom: rc['EUROPE'].zoom,
	center: new google.maps.LatLng(rc['EUROPE'].lat, rc['EUROPE'].lan),
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	panControl: false,
	//zoomControl: false,
	mapTypeControl: false,
	scaleControl: false,
	streetViewControl: false,
	overviewMapControl: false,
	rotateControl: false,
	minZoom: 1,
	maxZoom: 5,
});
var markerCluster = new MarkerClusterer(map, null, {
	styles: icon_style,
	minimumClusterSize: 1,
	zoomOnClick: false,
});
fn.fill_marker_groups();
fn.marker_control();

// Dom Listener
$('#marker-filter').on('click', '.marker-group', fn.marker_control);
$('#marker-filter').on('click', '.zoom', fn.zoom);

});
