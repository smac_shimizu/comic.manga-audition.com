jQuery(function($) {
	$("a.ajax").colorbox({href:function(){return $(this).attr('href') + " .entry-content";}, height:"100%", width: "940px"});
	$(document).on('cbox_complete', function(){ 
		$('#cboxLoadedContent').scrollTo($('.entry-meta'), 'normal');
		$('#cboxLoadedContent').on('click', 'img', function(e){
			$('#cboxLoadedContent').scrollTo($(this).nextUntil('img').last().next(), 'normal');
		});
	});
});
