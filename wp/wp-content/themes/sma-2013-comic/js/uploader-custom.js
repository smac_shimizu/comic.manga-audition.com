jQuery(function($) {

	// Init
	// -----------------------------------
	var fn = {};
	var zipRequired = false;

	// Misc
	// -----------------------------------
	$('#entry-class-button').click(function() {
		if($('#entry_class').val()) {
			$('#entry-class-button').attr('disabled', 'disabled').hide();
			zipRequired = $('#entry_class option:selected').data('zip-required');
			$('#entry_class').attr('disabled', 'disabled');
			$('#uploader-block').fadeIn('slow');
		}
	});

	$('#entry-form').submit(function() {
		$('#entry_class').removeAttr('disabled');
	});

	// Plupload
	// -----------------------------------
	plupload.addFileFilter('allowed_chars', function(regex, file, cb) {
		if(!regex.test(file.name)) {
			this.trigger('Error', {
				code : 'FILE_NAME_ERROR',
				message : 'Error: Filename may only include alphanumeric characters, dots, hyphens and underscores',
				file : file
			});
			cb(false);
		}
		else
			cb(true);
	});

	plupload.addFileFilter('max_image_file_size', function(maxSize, file, cb) {
		var undef;
		var maxSizeOrig = maxSize;
		maxSize = plupload.parseSize(maxSize) || 0;

		// Invalid file size
		if ((/\.(jpg|png)$/i).test(file.name) && file.size !== undef && maxSize && file.size > maxSize) {
			this.trigger('Error', {
				code : 'IMAGE_FILE_SIZE_ERROR',
				message : 'Error: The maximum file size for images is ' + maxSizeOrig,
				file : file
			});
			cb(false);
		}
		else
			cb(true);
	});

	fn.zipCheck = function(up, files) {
		var showStartButton = false;

		plupload.each(up.files, function(file) {
			if(zipRequired && (/\.zip$/i).test(file.name))
				showStartButton = true;
			else if(!zipRequired)
				showStartButton = true;
		});

		if(showStartButton)
			$('.plupload_button.plupload_start').show();
		else
			$('.plupload_button.plupload_start').hide();
	}

	// Setup html5 version
	$("#uploader").pluploadQueue({
		// General settings
		runtimes : 'html5',
		url : '/wp/wp-content/themes/sma-2013-comic/upload.php',
		max_file_size: '200mb',
		chunks : {
			size: '1mb',
			send_chunk_number: false // set this to true, to send chunk and total chunk numbers instead of offset and total bytes
		},
		rename : true,
		dragdrop: true,
		filters : {
			max_image_file_size: '5mb',
			// Specify what files to browse for
			mime_types: [
				{title : "Image files", extensions : "jpg,png"},
				{title : "Zip files", extensions : "zip"}
			],
			prevent_duplicates: true,
			allowed_chars: /^[0-9a-zA-Z-_\.]+$/
		},
		init: {
			PostInit: function() {
				$(".plupload_button.plupload_start").hide();
			},
			UploadComplete: function (up, files) {
				if ($("#uploader_count").val() > 0)
					$('#details').fadeIn('slow');
			},
			FilesAdded: function(up, files) {
				fn.zipCheck(up, files);
			},
			FilesRemoved: function(up, files) {
				fn.zipCheck(up, files);
			},
			Error: function(up, err) {
				if (err.code == 'FILE_NAME_ERROR') {
					alert(err.message + ': ' + err.file.name);
				}
				if (err.code == 'IMAGE_FILE_SIZE_ERROR') {
					alert(err.message + ': ' + err.file.name);
				}
			}
		},
		multipart_params : {
			fuid : $("#fuid").val()
		}
	});
});
