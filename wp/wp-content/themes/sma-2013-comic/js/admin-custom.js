jQuery(function($) {

// Init
// -----------------------------------
var fn = {};

// Functions
// -----------------------------------
fn.isPage = function(name) {
	if (typeof this.bodyClass === 'undefined')
		this.bodyClass = $('body').attr('class').split(' ');
	return $.inArray(name, this.bodyClass) !== -1 ? true : false;
}

// Page specific codes
// -----------------------------------

// user-edit-php
if(fn.isPage('user-edit-php')) {
	$('.update-nag').hide();
	$('h3:contains("Personal Options")').hide().next('.form-table').hide();
}

});
