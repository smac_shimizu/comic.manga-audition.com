<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/

$sma2013comic_field_value = sma2013comic_get_users_profile_field_values();
?>
<div class="login profile" id="theme-my-login<?php $template->the_instance(); ?>">
	<?php $template->the_action_template_message( 'profile' ); ?>
	<?php $template->the_errors(); ?>
	<form id="your-profile" action="<?php $template->the_action_url( 'profile' ); ?>" method="post">
		<?php wp_nonce_field( 'update-user_' . $current_user->ID ); ?>
		<p>
			<input type="hidden" name="from" value="profile" />
			<input type="hidden" name="checkuser_id" value="<?php echo $current_user->ID; ?>" />
		</p>

		<?php if ( has_action( 'personal_options' ) ) : ?>

		<h3><?php _e( 'Personal Options' ); ?></h3>

		<table class="form-table">
		<?php do_action( 'personal_options', $profileuser ); ?>
		</table>

		<?php endif; ?>

		<?php do_action( 'profile_personal_options', $profileuser ); ?>

		<h3><?php _e( 'Name' ); ?></h3>

		<table class="form-table">
		<tr>
			<th><label for="user_login"><?php _e( 'Username' ); ?></label></th>
			<td><input type="text" name="user_login" id="user_login" value="<?php echo esc_attr( $profileuser->user_login ); ?>" disabled="disabled" class="regular-text" /> <span class="description"><?php _e( 'Your username cannot be changed.', 'theme-my-login' ); ?></span></td>
		</tr>

		<tr>
			<th><label for="first_name"><?php _e( 'First Name' ); ?></label></th>
			<td><input type="text" name="first_name" id="first_name" value="<?php echo esc_attr( $sma2013comic_field_value->first_name ); ?>" class="regular-text" /></td>
		</tr>

		<tr>
			<th><label for="last_name"><?php _e( 'Last Name' ); ?></label></th>
			<td><input type="text" name="last_name" id="last_name" value="<?php echo esc_attr( $sma2013comic_field_value->last_name ); ?>" class="regular-text" /></td>
		</tr>

		<tr>
			<th><label for="nickname">Author Name Displayed As</label></th>
			<td><input type="text" name="nickname" id="nickname" value="<?php echo esc_attr( $sma2013comic_field_value->nickname ); ?>" class="regular-text" /></td>
		</tr>
		</table>

		<h3><?php _e( 'Contact Info' ); ?></h3>

		<table class="form-table">
		<tr>
			<th><label for="email"><?php _e( 'E-mail' ); ?></label></th>
			<td><input type="text" name="email" id="email" value="<?php echo esc_attr( $sma2013comic_field_value->user_email ); ?>" class="regular-text" /></td>
		</tr>

		<?php
		$sma2013comic_contactmethods = array();
		foreach ( _wp_get_user_contactmethods() as $name => $desc ) {
			$sma2013comic_contactmethods[$name] = array();
			$sma2013comic_contactmethods[$name]['desc'] = $desc;
			$sma2013comic_contactmethods[$name]['html'] = '
			<tr>
			<th><label for="' . $name . '">' . apply_filters( 'user_'.$name.'_label', $desc ) . '</label></th>
			<td><input type="text" name="' . $name . '" id="' . $name . '" value="' . esc_attr( $sma2013comic_field_value->$name ) . '" class="regular-text" /></td>
			</tr>
			';
		}
		?>
		<tr>
			<th><label for="country_code"><?php echo apply_filters( 'user_country_code_label', $sma2013comic_contactmethods['country_code']['desc'] ); ?></label></th>
			<td>
				<select name="country_code" id="country_code">
					<option value="">Select Country</option>
				<?php
					foreach (sma2013comic_get_country_code_info(null, null, true) as $code => $info ) {
						if('(NOT INCLUDED)' == $info['region'] || 'ORPHANED' == $info['region'])
							continue;
				?>
					<option value="<?php echo esc_attr($code); ?>"<?php selected($sma2013comic_field_value->country_code, $code); ?>><?php echo esc_html($info['name']); ?></option>
				<?php
					}
				?>
				</select>
			</td>
		</tr>
		<?php echo $sma2013comic_contactmethods['postal_address']['html']; ?>
		<?php echo $sma2013comic_contactmethods['phone_number']['html']; ?>
		</table>

		<h3><?php _e( 'About Yourself' ); ?></h3>

		<table class="form-table">

		<tr>
			<th><label for="date_of_birth"><?php echo apply_filters( 'user_date_of_birth_label', $sma2013comic_contactmethods['date_of_birth']['desc'] ); ?></label></th>
			<td><input type="text" name="date_of_birth" id="date_of_birth" value="<?php echo esc_attr( $sma2013comic_field_value->date_of_birth ); ?>" class="regular-text" placeholder="yyyy/mm/dd" /></td>
		</tr>

		<?php echo $sma2013comic_contactmethods['occupation']['html']; ?>
		<tr>
			<th><label for="projects"><?php echo apply_filters( 'user_projects_label', $sma2013comic_contactmethods['projects']['desc'] ); ?></label></th>
			<td><textarea name="projects" id="projects" rows="5" cols="30"><?php echo esc_html( $sma2013comic_field_value->projects ); ?></textarea></td>
		</tr>
		<tr>
			<th><label for="description">Biography</label></th>
			<td><textarea name="description" id="description" rows="5" cols="30"><?php echo esc_html( $sma2013comic_field_value->description ); ?></textarea></td>
		</tr>
		<tr>
			<th><label for="where_did_you_hear_about_us"><?php echo apply_filters( 'user_where_did_you_hear_about_us_label', $sma2013comic_contactmethods['where_did_you_hear_about_us']['desc'] ); ?></label></th>
			<td><textarea name="where_did_you_hear_about_us" id="where_did_you_hear_about_us" rows="5" cols="30"><?php echo esc_html( $sma2013comic_field_value->where_did_you_hear_about_us ); ?></textarea></td>
		</tr>
		</table>

		<table class="form-table">
		<?php
		$show_password_fields = apply_filters( 'show_password_fields', true, $profileuser );
		if ( $show_password_fields ) :
		?>
		<tr id="password">
			<th><label for="pass1"><?php _e( 'New Password' ); ?></label></th>
			<td><input type="password" name="pass1" id="pass1" size="16" value="" autocomplete="off" /> <span class="description"><?php _e( 'If you would like to change the password type a new one. Otherwise leave this blank.' ); ?></span><br />
				<input type="password" name="pass2" id="pass2" size="16" value="" autocomplete="off" /> <span class="description"><?php _e( 'Type your new password again.' ); ?></span><br />
				<div id="pass-strength-result"><?php _e( 'Strength indicator', 'theme-my-login' ); ?></div>
				<p class="description indicator-hint"><?php _e( 'Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers and symbols like ! " ? $ % ^ &amp; ).' ); ?></p>
			</td>
		</tr>
		<?php endif; ?>
		</table>

		<?php do_action( 'show_user_profile', $profileuser ); ?>

		<p class="submit">
			<input type="hidden" name="display_name" value="<?php echo esc_attr( $sma2013comic_field_value->nickname ); ?>" />
			<input type="hidden" name="action" value="profile" />
			<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
			<input type="hidden" name="user_id" id="user_id" value="<?php echo esc_attr( $current_user->ID ); ?>" />
			<input type="submit" class="button-primary" value="<?php esc_attr_e( 'Update Profile' ); ?>" name="submit" />
		</p>
	</form>
</div>
