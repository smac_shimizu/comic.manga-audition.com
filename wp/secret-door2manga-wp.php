<?php
/*
 Version 1.1
 Copyright (c) 2015 Tinybit Inc.

@require_once('hard_to_guess_name.php');
tbcmm();
*/

class TB_Cookie_Maintenance_Mode {
	var $name = '';
	var $value = '';
	var $env = array();

	function set() {
		setcookie($this->name, $this->value, 0, '/', $this->env['HTTP_HOST']);

		// More restricted way
		//setcookie($this->name, $this->value, 0, dirname($this->env['SCRIPT_NAME']) . '/');
	}

	function validate() {
		return isset($_COOKIE[$this->name]) && $this->value == $_COOKIE[$this->name];
	}

	function status_header($code, $description) {
		$protocol = $this->env['SERVER_PROTOCOL'];
		$protocol = 'HTTP/1.1' != $protocol && 'HTTP/1.0' != $protocol ? 'HTTP/1.0' : $protocol;
		@header("$protocol $code $description", true, $code);
	}

	function redirect($location, $status = 302) {
		if ( ! $location )
			return false;
		header("Location: $location", true, $status);
		return true;
	}

	function get_parent_directory_url() {
		return 'http://' . $this->env['HTTP_HOST'] . dirname($this->env['REQUEST_URI']) . '/';
	}

	function get_json_data($file, $assoc = false) {
		return json_decode(@file_get_contents($file), $assoc);
	}

	function is_ip_automattic($ip, $data = array()) {
		$data = (array) $data;
		$long = ip2long($ip);
		$long = $long > 0 ? $long : $long + 4294967296;

		foreach ($data as $v) {
			if($v->s <= $long && $long <= $v->e)
				return true;
		}

		return false;
	}

	function is_remote_addr_blacklisted($ips = array()) {
		$ips = (array) $ips;

		foreach($ips as $ip) {
			if(0 === strpos($this->env['REMOTE_ADDR'], $ip))
				return true;
		}

		return false;
	}

	function utf8_exit($message = '') {
		@header('Content-Type: text/html; charset=utf-8');
		echo $message;
		exit;
	}

	function set_env() {
		$this->env['HTTP_HOST'] = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
		$this->env['HTTP_USER_AGENT'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
		$this->env['REMOTE_ADDR'] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
		$this->env['REQUEST_URI'] = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
		$this->env['SCRIPT_NAME'] = isset($_SERVER['SCRIPT_NAME']) ? $_SERVER['SCRIPT_NAME'] : '';
		$this->env['SERVER_ADDR'] = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';
		$this->env['SERVER_PROTOCOL'] = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : '';
	}

	function __construct() {
		$this->set_env();
		$file = md5(__FILE__);
		$this->name = md5($this->env['HTTP_USER_AGENT'] . $this->env['REMOTE_ADDR'] . $file);
		$this->value = md5($this->env['REMOTE_ADDR'] . $this->env['SERVER_ADDR'] . $file);
	}
}

function tbcmm($args = array()) {
	global $TBCMM;

	$args = array_merge(array(
		'maintenance' => false,
		'maintenance_retry' => 7200,
		'maintenance_message' => 'Under Maintenance',
		'maintenance_message_file_path' => null,
		'disable_xmlrpc' => true,
		'allow_automattic_ips' => false,
		'automattic_ips_json' => dirname(__FILE__) . '/wp-content/_automattic-ips.json',
		'failed_login_redirect_location' => $TBCMM->get_parent_directory_url(),
		'failed_login_message' => 'Access Denied',
		'blacklisted_ips' => null,
		'blacklisted_ips_message' => 'Access Denied',
	), $args);

	// Block blacklisted ips
	if($args['blacklisted_ips'] && is_array($args['blacklisted_ips']) && $TBCMM->is_remote_addr_blacklisted($args['blacklisted_ips'])) {
		$TBCMM->status_header(403, 'Forbidden');
		$TBCMM->utf8_exit($args['blacklisted_ips_message']);
	}

	if(defined('WP_CLI'))
		return;

	if($TBCMM->validate())
		return;

	// Maintenance mode
	if($args['maintenance']) {
		$TBCMM->status_header(503, 'Service Unavailable');
		@header('Retry-After: ' . $args['maintenance_retry']);

		if($args['maintenance_message_file_path'])
			$TBCMM->utf8_exit(file_get_contents($args['maintenance_message_file_path']));

		$TBCMM->utf8_exit($args['maintenance_message']);
	}

	/* Disable xmlrpc
		Better use with these:
		add_filter('xmlrpc_enabled', '__return_false');
		add_filter('xmlrpc_methods', function($methods) {
			unset($methods['pingback.ping']);
			return $methods;
		});
	*/
	if($args['disable_xmlrpc'] && defined('XMLRPC_REQUEST') && XMLRPC_REQUEST) {
		$disable_xmlrpc = true;

		if($args['allow_automattic_ips'] && $TBCMM->is_ip_automattic($TBCMM->env['REMOTE_ADDR'], $TBCMM->get_json_data($args['automattic_ips_json'])))
			$disable_xmlrpc = false;

		if($disable_xmlrpc) {
			$TBCMM->status_header(403, 'Forbidden');
			exit(' ');
		}
	}

	// Secret login needed
	if('wp-login.php' == basename($TBCMM->env['SCRIPT_NAME']) || (defined('WP_ADMIN') && 'admin-ajax.php' != basename($TBCMM->env['SCRIPT_NAME']))) {
		if($args['failed_login_redirect_location']) {
			$TBCMM->redirect($args['failed_login_redirect_location']);
			exit;
		}

		$TBCMM->status_header(403, 'Forbidden');
		$TBCMM->utf8_exit($args['failed_login_message']);
	}
}

global $TBCMM;
$TBCMM = new TB_Cookie_Maintenance_Mode;

// Standalone access
if(count(get_included_files()) === 1) {
	$TBCMM->set();
	$TBCMM->redirect($TBCMM->get_parent_directory_url() . 'wp-login.php');
	exit;
}
